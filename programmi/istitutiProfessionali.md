# ISTITUTI PROFESSIONALI

## INFORMAZIONI GENERALI

L'area di istruzione generale ha l'obiettivo di fornire ai giovani la preparazione di base, acquisita attraverso il rafforzamento e lo sviluppo degli assi culturali, che caratterizzano l'obbligo di istruzione: asse dei linguaggi, matematico, scientifico-tecnologico, storico-sociale.
Gli studenti degli istituti professionali conseguono la propria preparazione di base con l'uso sistematico di metodi che, attraverso la personalizzazione dei percorsi, valorizzano l'apprendimento incontesti formali, non formali e informali.
Le aree di indirizzo, presenti sin dal primo biennio, hanno l'obiettivo di far acquisire agli studenti competenze spendibili in vari contesti di vita e di lavoro, mettendo i diplomati in grado di assumere autonome responsabilità nei processi produttivi e di servizio e di collaborare costruttivamente alla soluzione di problemi.
Le attività e gli insegnamenti relativi a "Cittadinanza e Costituzione" di cui all’art. 1 del decreto legge 1 settembre 2008, n. 137, convertito con modificazioni, dalla legge 30 ottobre 2008, n. 169, coinvolgono tutti gli ambiti disciplinari e si sviluppano, in particolare, in quelli di interesse storico-sociale e giuridico-economico.
Assume particolare importanza nella progettazione formativa degli istituti professionali la scelta metodologica dell'alternanza scuola lavoro, che consente pluralità di soluzioni didattiche e favorisce il collegamento con il territorio. 
I risultati di apprendimento, attesi a conclusione del percorso quinquennale, consentono agli studenti di inserirsi nel mondo del lavoro, di proseguire nel sistema dell'istruzione e formazione tecnica superiore, nei percorsi universitari, nonché nei percorsi di studio e di lavoro previsti per l'accesso agli albi delle professioni tecniche secondo le norme vigenti in materia.
A tale scopo, viene assicurato nel corso del quinquennio un orientamento permanente che favorisca da parte degli studenti scelte fondate e consapevoli.

## PECUP TUTTI I PERCORSI

 A conclusione dei percorsi degli istituti professionali, gli studenti sono in grado di:
 
 - agire in riferimento ad un sistema di valori, coerenti con i principi della Costituzione, in base ai quali essere in grado di valutare fatti e orientare i propri comportamenti personali, sociali e professionali;
 - utilizzare gli strumenti culturali e metodologici acquisiti per porsi con atteggiamento razionale, critico, creativo e responsabile nei confronti della realtà, dei suoi fenomeni e dei suoi problemi, anche ai fini dell’apprendimento permanente;
 - utilizzare il patrimonio lessicale ed espressivo della lingua italiana secondo le esigenze comunicative nei vari contesti: sociali, culturali, scientifici, economici, tecnologici e professionali;
 - riconoscere le linee essenziali della storia delle idee, della cultura, della letteratura, delle arti e orientarsi agevolmente fra testi e autori fondamentali, a partire dalle componenti di natura tecnico-professionale correlate ai settori di riferimento;
 - riconoscere gli aspetti geografici, ecologici, territoriali, dell’ambiente naturale ed antropico, le connessioni con le strutture demografiche, economiche, sociali, culturali e le trasformazioni intervenute nel corso del tempo;
 - stabilire collegamenti tra le tradizioni culturali locali, nazionali ed internazionali, sia in una prospettiva interculturale sia ai fini della mobilità di studio e di lavoro;
 - utilizzare i linguaggi settoriali delle lingue straniere previste dai percorsi di studio per interagire in diversi ambiti e contesti di studio e di lavoro;
 - riconoscere il valore e le potenzialità dei beni artistici e ambientali;
 - **individuare ed utilizzare le moderne forme di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete;**
 - **utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare;**
 - riconoscere i principali aspetti comunicativi, culturali e relazionali dell’espressività corporea ed esercitare in modo efficace la pratica sportiva per il benessere individuale e collettivo;
 - comprendere e utilizzare i principali concetti relativi all'economia, all'organizzazione, allo svolgimento dei processi produttivi e dei servizi;
 - utilizzare i concetti e i fondamentali strumenti delle diverse discipline per comprendere la realtà ed operare in campi applicativi;
 - **padroneggiare l'uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell'ambiente e del territorio;**
 - individuare i problemi attinenti al proprio ambito di competenza e impegnarsi nella loro soluzione collaborando efficacemente con gli altri;
 - utilizzare strategie orientate al risultato, al lavoro per obiettivi e alla necessità di assumere responsabilità nel rispetto dell'etica e della deontologia professionale;
 - compiere scelte autonome in relazione ai propri percorsi di studio e di lavoro lungo tutto l'arco della vita nella prospettiva dell'apprendimento permanente;
 - partecipare attivamente alla vita sociale e culturale a livello locale, nazionale e comunitario. 

 ## PECUP SETTORE SERVIZI

 - riconoscere nell'evoluzione dei processi dei servizi, le componenti culturali,sociali, economiche e tecnologiche che li caratterizzano, in riferimento ai diversi contesti, locali e globali;
 - cogliere criticamente i mutamenti culturali, sociali, economici e tecnologici che influiscono sull'evoluzione dei bisogni e sull'innovazione dei processi di servizio;
 - essere sensibili alle differenze di cultura e di atteggiamento dei destinatari, al fine di fornire un servizio il più possibile personalizzato;
 - sviluppare ed esprimere le proprie qualità di relazione, comunicazione, ascolto, cooperazione e senso di responsabilità nell'esercizio del proprio ruolo;
 - svolgere la propria attività operando in équipe e integrando le proprie competenze con le altre figure professionali, al fine di erogare un servizio di qualità;
 - contribuire a soddisfarele esigenze del destinatario, nell’osservanza degli aspetti deontologici del servizio;
 - applicare le normative che disciplinano i processi dei servizi, con riferimento alla riservatezza, alla sicurezza e salute sui luoghi di vita e di lavoro, alla tutela e alla valorizzazione dell'ambiente e del territorio;
 - **intervenire, per la parte di propria competenza e con l’utilizzo di strumenti tecnologici, nelle diverse fasi e livelli del processo per la produzione della documentazione richiesta e per l’esercizio del controllo di qualità.**

 ## PECUP SETTORE INDUSTRIA E ARTIGIANATO

- riconoscere nell'evoluzione dei processi produttivi, le componenti scientifiche, economiche, tecnologiche e artistiche che li hanno determinati nel corso della storia, con riferimento sia ai diversi contesti locali e globali sia ai mutamenti delle condizioni di vita;
- utilizzare le tecnologie specifiche del settore e sapersi orientare nella normativa di riferimento;
- applicare le normative che disciplinano i processi produttivi, con riferimento alla riservatezza, alla sicurezza e salute sui luoghi di vita e di lavoro, alla tutela e alla valorizzazione dell'ambiente e del territorio;
- **intervenire, per la parte di propria competenza e con l’utilizzo di strumenti tecnologici, nelle diverse fasi e livelli del processo dei servizi, per la produzione della documentazione richiesta e per l’esercizio del controllo di qualità;**
- svolgere la propria attività operando in équipe, integrando le proprie competenze all'interno di un dato processo produttivo;
- riconoscere e applicare i principi dell'organizzazione, della gestione e del controllo dei diversi processi produttivi assicurando i livelli di qualità richiesti;
- riconoscere e valorizzare le componenti creative in relazione all'ideazione di processi e prodotti innovativi nell'ambito industriale e artigianale;
- comprendere le implicazioni etiche, sociali, scientifiche, produttive, economiche, ambientali dell'innovazione tecnologica e delle sue applicazioni industriali, artigianali e artistiche. 

### QUADRO ORARIO

|                        | 1    | 2    | 3    | 4    | 5    |
| ---------------------- | ---- | ---- | ---- | ---- | ---- |
| Insegnamenti generali  | 660  | 660  | 495  | 495  | 495  |
| Insegnamenti indirizzo | 396  | 396  | 561  | 561  | 561  |
| Totale                 | 1056 | 1056 | 1056 | 1056 | 1056 |

## DISCIPLINE A041

### QUADRO ORARIO

|                                                                  | Disciplina                                            | 1   | 2   | 3   | 4   | 5   |
| ---------------------------------------------------------------- | ----------------------------------------------------- | --- | --- | --- | --- | --- |
| SETTORE SERVIZI - SERVIZI COMMERCIALI                            | Informatica e laboratorio                             | 66  | 66  |     |     |     |
| SETTORE SERVIZI - SERVIZI PER L'AGRICOLTURA E LO SVILUPPO RURALE | Tecnologie dell'informazione e della comunicazione    | 66  | 66  |     |     |     |
| INDUSTRIA E ARTIGIANATO                                          | Tecnologie dell'informazione e della comunicazione    | 66  | 66  |     |     |     |
| INDUSTRIA E ARTIGIANATO - MANUTENZIONE E ASSISTENZA TECNICA      | Tecnologie e tecniche di installazione e manutenzione |     |     | 99  | 165 | 264 |


### TECNOLOGIE DELL'INFORMAZIONE E DELLA COMUNICAZIONE - SERVIZI PER L'AGRIGOLTURA E LO SVILUPPO RURALE

Il docente di "Tecnologie dell’informazione e della comunicazione" concorre a far conseguire allo studente, al termine del percorso quinquennale di istruzione professionale del settore "Servizi", indirizzo "Servizi per l’agricoltura e lo sviluppo rurale", risultati di apprendimento che lo mettono in grado di: utilizzare e produrre strumenti di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete; utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; individuare e utilizzare gli strumenti di comunicazione e di team working più appropriati per intervenire nei contesti organizzativi e professionali di riferimento.

**Conoscenze**

- analizzare dati e interpretarli sviluppando deduzioni e ragionamenti sugli stessi anche con l’ausilio di rappresentazioni grafiche , usando consapevolmente gli strumenti di calcolo e le potenzialità offerte da applicazioni specifiche di tipo informatico
- essere consapevole delle potenzialità e dei limiti delle tecnologie nel contesto culturale e sociale in cui vengono applicate
- utilizzare e produrre testi multimediali 

**Competenze**

- Informazioni, dati e loro codifica.
- Architettura e componenti di un computer.
- Funzioni di un sistema operativo.
- Software di utilità e software applicativi.
- Concetto di algoritmo.
- Fasi risolutive di un problema e loro rappresentazione.
- Tecniche di rappresentazione di testi, dati e funzioni.
- Funzioni e caratteristiche della rete internet.
- Normativa sulla privacy e sul diritto d’autore.
- Tecniche di rappresentazione di testi, dati e funzioni.
- Sistemi di documentazione e archiviazione di progetti, disegni e materiali informativi.
- Fattori che influenzano una produzione.
- Forme di comunicazione commerciale e pubblicità.
- Tecniche di presentazione.
- Elementi principali dei sistemi informativi.

**Abilità**

- Riconoscere le caratteristiche funzionali di un computer (calcolo, elaborazione, comunicazione).
- Riconoscere ed utilizzare le funzioni di base di un sistema operativo.
- Utilizzare applicazioni elementari di scrittura, calcolo e grafica.
- Raccogliere, organizzare e rappresentare informazioni.
- Utilizzare gli strumenti informatici nelle applicazioni d’interesse, nelle discipline di area generale e di indirizzo.
- Utilizzare la rete internet per ricercare fonti e dati.
- Utilizzare la rete per attività di comunicazione interpersonale.
- Riconoscere i limiti e i rischi dell’uso della rete.
- Leggere e costruire schemi a blocchi.
- Rappresentare dati e funzioni.
- Individuare i principali strumenti di gestione per la diffusione e commercializzazione di un prodotto industriale o artigianale.
- Descrivere le principali funzioni di un’azienda.

### INFORMATICA E LABORATORIO - SERVIZI COMMERCIALI

Il docente di "Informatica e laboratorio" concorre a far conseguire allo studente, al termine del percorso quinquennale di istruzione professionale del settore "Servizi commerciali", risultati di apprendimento che lo mettono in grado di: utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; utilizzare e produrre strumenti di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete; padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio; svolgere attività connesse all’attuazione delle rilevazioni aziendali con l’utilizzo di strumenti tecnologici e software applicativi di settore; interagire col sistema informativo aziendale anche attraverso l’uso di strumenti informatici e telematici.

**Competenze**

- individuare le strategie appropriate per la soluzione di problemi
- utilizzare e produrre testi multimediali
- analizzare dati e interpretarli sviluppando deduzioni e ragionamenti sugli stessi anche con l’ausilio di rappresentazioni grafiche, usando consapevolmente gli strumenti di calcolo e le potenzialità offerte da applicazioni specifiche di tipo informatico
- essere consapevole delle potenzialità e dei limiti delle tecnologie nel contesto culturale e sociale in cui vengono applicate 

**Conoscenze**

- Sistemi informatici.
- Informazioni, dati e loro codifica.
- Architettura e componenti di un computer.
- Comunicazione uomo-macchina.
- Struttura e funzioni di un sistema operativo.
- Software di utilità e software gestionali.
- Fasi risolutive di un problema, algoritmi e loro rappresentazione.
- Organizzazione logica dei dati.
- Struttura di una rete.
- Funzioni e caratteristiche della rete Internet e della posta elettronica.
- Normativa sulla privacy e sul diritto d’autore.

**Abilità**

- Riconoscere le caratteristiche logico-funzionali di un computer e il ruolo strumentale svolto nei vari ambiti (calcolo, elaborazione, comunicazione, ecc.).
- Utilizzare le funzioni di base di un sistema operativo.
- Organizzare dati/informazioni sia di tipo testuale che multimediale.
- Utilizzare programmi di scrittura, di grafica e il foglio elettronico.
- Utilizzare software gestionali per le attività del settore di studio.
- Utilizzare la rete Internet per ricercare fonti e dati di tipo tecnico-economico.
- Utilizzare le reti per attività di comunicazione interpersonale.
- Riconoscere i limiti e i rischi dell’uso della tecnologie.
- Riconoscere le principali forme di gestione e controllo dell’informazione e della comunicazione specie nell’ambito tecnico-economico. 

## TECNOLOGIE DELL'INFORMAZIONE E DELLA COMUNICAZIONE - INDUSTRIA E ARTIGIANATO INDIRIZZO PRODUZIONI INDUSTRIALI E ARTIGIANALI

## TECNOLOGIE DELL'INFORMAZIONE E DELLA COMUNICAZIONE - INDUSTRIA E ARTIGIANATO INDIRIZZO MANUTENZIONE E ASSISTENZA TECNICA

## TECNOLOGIE E TECNICHE DI INSTALLAZIONE E MANUTENZIONE - INDUSTRIA E ARTIGIANATO INDIRIZZO MANUTENZIONE E ASSISTENZA TECNICA

Il docente di "Tecnologie e tecniche di installazione e di manutenzione" concorre a far conseguire allo studente, al termine del percorso quinquennale, i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale: padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio; individuare i problemi attinenti al proprio ambito di competenza e impegnarsi nella loro soluzione collaborando efficacemente con gli altri; utilizzare strategie orientate al risultato, al lavoro per obiettivi e alla necessità di assumere responsabilità nel rispetto dell’etica e della deontologia professionale; utilizzare le tecnologie specifiche del settore e sapersi orientare nella normativa di riferimento; intervenire, per la parte di propria competenza e con l’utilizzo di strumenti tecnologici, nelle diverse fasi e livelli del processo dei servizi, per la produzione della documentazione richiesta e per l’esercizio del controllo di qualità

**Competenze**

- utilizzare, attraverso la conoscenza e l’applicazione della normativa sulla sicurezza, strumenti e tecnologie specifiche;
- utilizzare la documentazione tecnica prevista dalla normativa per garantire la corretta funzionalità di apparecchiature, impianti e sistemi tecnici per i quali cura la manutenzione;
- individuare i componenti che costituiscono il sistema e i vari materiali impiegati, allo scopo di intervenire nel montaggio, nella sostituzione dei componenti e delle parti, nel rispetto delle modalità e delle procedure stabilite;
- garantire e certificare la messa a punto degli impianti e delle macchine a regola d’arte, collaborando alla fase di collaudo e di installazione;
- gestire le esigenze del committente, reperire le risorse tecniche e tecnologiche per offrire servizi efficaci ed economicamente correlati alle richieste;
- analizzare il valore, i limiti e i rischi delle varie soluzioni tecniche per la vita sociale e culturale con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio.

#### SECONDO BIENNIO

**Conoscenze**

- Specifiche tecniche e funzionali dei componenti e dei dispositivi
- Tecniche e procedure di assemblaggio e di installazione di impianti e di apparati o dispositivi meccanici, elettrici ed elettronici.
- Tecniche e procedure di installazione di circuiti oleodinamici e pneumatici
- Tecniche e procedure di montaggio di apparecchiature elettriche e sistemi di protezione
- Norme sulla sicurezza e sulla tutela ambientale
- Procedure generali di collaudo e di esercizio
- Livelli di manutenzione
- Classificazione degli interventi manutentivi 
- Struttura dei manuali di manutenzione
- Caratteristiche di funzionamento e specifiche di macchine e impianti meccanici, termici, elettrici ed elettronici.
- Certificazione di Qualità ed enti certificatori
- Diagnostica del guasto e procedure di intervento
- Documentazione tecnica di interesse
- Affidabilità di componenti e sistemi
- Disponibilità delle risorse sufficienti

**Abilità**

- Riconoscere e designare i principali componenti
- Interpretare i dati e le caratteristiche tecniche dei componenti di apparati e impianti
- Assemblare e installare impianti, dispositivi e apparati
- Osservare le norme di tutela della salute e dell’ambiente nelle operazioni di collaudo, esercizio e manutenzione
- Adottare i dispositivi di prevenzione e protezione prescritti dalle norme per la sicurezza nell’ambiente di lavoro
- Interpretare i contenuti delle certificazioni Individuare i criteri per l’esecuzioni dei collaudi dei dispositivi.
- Verificare la corrispondenza delle caratteristiche rilevate alle specifiche tecniche dichiarate.
- Redigere la documentazione e le attestazioni obbligatorie
- Procedure negli interventi di manutenzione
- Effettuare visite tecniche e individuare le esigenze d’intervento
- Individuare le risorse strumentali necessarie all’erogazione del servizio 
- Eseguire interventi di manutenzione ed effettuare il collaudo
- Stimare i costi relativi all’intervento

#### QUINTO ANNO

**Conoscenze**

- Metodiche di ricerca e diagnostica dei guasti
- Procedure operative di smontaggio, sostituzione e rimontaggio di apparecchiature e impianti
- Modalità di compilazione dei documenti di collaudo
- Modalità di compilazione di documenti relativi alle normative nazionale ed europea di settore
- Documentazione per la certificazione della qualità
- Analisi di Affidabilità, Disponibilità, Manutenibilità e Sicurezza
- Linee guida del progetto di manutenzione.
- Tecniche per la programmazione di progetto
- Strumenti per il controllo temporale delle risorse e delle attività
- Elementi della contabilità generale e industriale
- Contratto di manutenzione e assistenza tecnica
- Principi, tecniche e strumenti della telemanutenzione e della teleassistenza
- Metodi tradizionali e innovativi di manutenzione
- Sistemi basati sulla conoscenza e sulla diagnosi multisensore
- Affidabilità del sistema di diagnosi.
- Lessico di settore, anche in lingua inglese

**Abilità**

- Ricercare e individuare guasti
- Smontare, sostituire e rimontare componenti e apparecchiature di varia tecnologia applicando procedure di sicurezza
- Applicare le procedure per il processo di certificazione di qualità
- Pianificare e controllare interventi di manutenzione
- Organizzare la logistica dei ricambi e delle scorte
- Gestire la logistica degli interventi
- Stimare i costi del servizio
- Redigere preventivi e compilare un capitolato di manutenzione
- Utilizzare, nei contesti operativi, metodi e strumenti di diagnostica tipici delle attività manutentive di interesse
- Utilizzare il lessico di settore, anche in lingua inglese.

**Abilità**