# ISTITUTI TECNICI

## INFORMAZIONI GENERALI

L’area di istruzione generale ha l’obiettivo di fornire ai giovani la preparazione di base, acquisita attraverso il rafforzamento e lo sviluppo degli assi culturali che caratterizzano l’obbligo di istruzione: asse dei linguaggi, matematico, scientifico-tecnologico, storico-sociale.
Le aree di indirizzo hanno l’obiettivo di far acquisire agli studenti sia conoscenze teoriche e applicative spendibili in vari contesti di vita,di studio e di lavoro sia abilità cognitive idonee per risolvere problemi, sapersi gestire autonomamente in ambiti caratterizzati da innovazioni continue, assumere progressivamente anche responsabilità per la valutazione e il miglioramento dei risultati ottenuti.
Le attività e gli insegnamenti relativi a “Cittadinanza e Costituzione” di cui all’art. 1 del decreto legge 1 settembre 2008 n. 137, convertito con modificazioni, dalla legge 30 ottobre 2008, n. 169, coinvolgono tutti gli ambiti disciplinari e si sviluppano, in particolare, in quelli di interesse storico-sociale e giuridico-economico.
I risultati di apprendimento attesi a conclusione del percorso quinquennale consentono agli studenti di inserirsi direttamente nel mondo del lavoro, di accedere all’università, al sistema dell’istruzione e formazione tecnica superiore, nonché ai percorsi di studio e di lavoro previsti per l’accesso agli albi delle professioni tecniche secondo le norme vigenti in materia.

## PECUP TUTTI I PERCORSI

A conclusione dei percorsi degli istituti tecnici, gli studenti attraverso lo studio, le esperienze operative di laboratorio e in contesti reali, la disponibilità al confronto e al lavoro cooperativo, la valorizzazione della loro creatività ed autonomia, 
sono in grado di:

- agire in base ad un sistema di valori coerenti con i principi della Costituzione, a partire dai quali saper valutare fatti e ispirare i propri comportamenti personali e sociali;
- utilizzare gli strumenti culturali e metodologici acquisiti per porsi con atteggiamento razionale, critico e responsabile di fronte alla realtà, ai suoi fenomeni e ai suoi problemi, anche ai fini dell’apprendimento permanente;
- padroneggiare il patrimonio lessicale ed espressivo della lingua italiana secondo le esigenze comunicative nei vari contesti: sociali, culturali, scientifici, economici, tecnologici;
- riconoscere le linee essenziali della storia delle idee, della cultura, della letteratura, delle arti e orientarsi agevolmente fra testi e autori fondamentali, con riferimento soprattutto a tematiche di tipo scientifico, tecnologico ed economico;
- riconoscere gli aspetti geografici, ecologici, territoriali, dell’ambiente naturale ed antropico, le connessioni con le strutture demografiche, economiche, sociali, culturali e le trasformazioni intervenute nel corso del tempo;
- stabilire collegamenti tra le tradizioni culturali locali, nazionali ed internazionali sia in una prospettiva interculturale sia ai fini della mobilità di studio e di lavoro;
- utilizzare i linguaggi settoriali delle lingue straniere previste dai percorsi di studio per interagire in diversi ambiti e contesti di studio e di lavoro;
- riconoscere il valore e le potenzialità dei beni artistici e ambientali, per una loro corretta fruizione e valorizzazione;
- **individuare ed utilizzare le moderne forme di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete**
- riconoscere gli aspetti comunicativi, culturali e relazionali dell’espressività corporea ed esercitare in modo efficace la pratica sportiva per il benessere individuale e collettivo;
- collocare le scoperte scientifiche e le innovazioni tecnologiche in una dimensione storico-culturale ed etica, nella consapevolezza della storicità dei saperi;
- **utilizzare modelli appropriati per investigare su fenomeni e interpretare dati sperimentali;**
- riconoscere, nei diversi campi disciplinari studiati, i criteri scientifici di affidabilità delle conoscenze e delle conclusioni che vi afferiscono;
- padroneggiare il linguaggio formale e i procedimenti dimostrativi della matematica; possedere gli strumenti matematici, statistici e del calcolo delle probabilità necessari per la comprensione delle discipline scientifiche e per poter operare nel campo delle scienze applicate;
- collocare il pensiero matematico e scientifico nei grandi temi dello sviluppo della storia delle idee, della cultura, delle scoperte scientifiche e delle invenzioni tecnologiche;
- **utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare;**
- **padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio;**
- utilizzare, in contesti di ricerca applicata, procedure e tecniche per trovare soluzioni innovative e migliorative, in relazione ai campi di propria competenza;
- cogliere l’importanza dell’orientamento al risultato, del lavoro per obiettivi e della necessità di assumere responsabilità nel rispetto dell’etica e della deontologia professionale;
- saper interpretare il proprio autonomo ruolo nel lavoro di gruppo;
- **analizzare criticamente il contributo apportato dalla scienza e dalla tecnologia allo sviluppo dei saperi e dei valori, al cambiamento delle condizioni di vita e dei modi di fruizione culturale;**
- essere consapevole del valore sociale della propria attività, partecipando attivamente alla vita civile e culturale a livello locale, nazionale e comunitario.

## PECUP INDIRIZZO ECONOMICO

Il profilo dei percorsi del settore economico si caratterizza per la cultura tecnico-economica riferita ad ampie aree: l’economia, l’amministrazione delle imprese, la finanza, il marketing, l’economia sociale e il turismo.
Gli studenti, a conclusione del percorso di studio, conoscono le tematiche relative ai macrofenomeni economico-aziendali, nazionali ed internazionali, alla normativa civilistica e fiscale, ai sistemi aziendali, anche con riferimento alla previsione, organizzazione, conduzione e controllo della gestione, agli strumenti di marketing, ai prodotti/servizi turistici.
In particolare, sono in grado di:

- analizzare la realtà e i fatti concreti della vita quotidiana ed elaborare generalizzazioni che aiutino a spiegare i comportamenti individuali e collettivi in chiave economica;
- riconoscere la varietà e lo sviluppo storico delle forme economiche, sociali e istituzionali attraverso le categorie di sintesi fornite dall’economia e dal diritto;
- riconoscere l’interdipendenza tra fenomeni economici, sociali, istituzionali, culturali e la loro dimensione locale/globale;
- **analizzare, con l’ausilio di strumenti matematici e informatici, i fenomeni economici e sociali;**
- orientarsi nella normativa pubblicistica, civilistica e fiscale;
- **intervenire nei sistemi aziendali con riferimento a previsione, organizzazione, conduzione e controllo di gestione;**
- utilizzare gli strumenti di marketing in differenti casi e contesti;
- distinguere e valutare i prodotti e i servizi aziendali, effettuando calcoli di convenienza per individuare soluzioni ottimali;
- **agire nel sistema informativo dell’azienda e contribuire sia alla sua innovazione sia al suo adeguamento organizzativo e tecnologico;**
- **elaborare, interpretare e rappresentare efficacemente dati aziendali con il ricorso a strumenti informatici e software gestionali;**
- analizzare i problemi scientifici, etici, giuridici e sociali connessi agli strumenti culturali acquisiti.

## PECUP INDIRIZZO TECNOLOGICO

Il profilo del settore tecnologico si caratterizza per la cultura tecnico-scientifica e tecnologica in ambiti ove interviene permanentemente l’innovazione dei processi, dei prodotti e dei servizi, delle metodologie di progettazione e di organizzazione. Gli studenti, a conclusione del percorso di studio, sono in grado di:

- **individuare le interdipendenze tra scienza, economia e tecnologia e le conseguenti modificazioni intervenute, nel corso della storia, nei settori di riferimento e nei diversi contesti, locali e globali;**
- **orientarsi nelle dinamiche dello sviluppo scientifico e tecnologico, anche con l’utilizzo di appropriate tecniche di indagine;**
- **utilizzare le tecnologie specifiche deivari indirizzi;**
- **orientarsi nella normativa che disciplina i processi produttivi del settore di riferimento, con particolare attenzione sia alla sicurezza sui luoghi di vita e di lavoro sia alla tutela dell’ambientee del territorio;**
- **intervenire nelle diverse fasi e livelli del processo produttivo, dall’ideazione alla realizzazione del prodotto, per la parte di propria competenza, utilizzando gli strumenti di progettazione, documentazione e controllo;**
- **riconoscere e applicare i principi dell’organizzazione, della gestione e del controllo dei diversi processi produttivi;**
- **analizzare criticamente il contributo apportato dalla scienza e dalla tecnologia allo sviluppo dei saperi e al cambiamento delle condizioni di vita;**
- **riconoscere le implicazioni etiche, sociali,scientifiche, produttive, economiche e ambientali dell’innovazione tecnologica e delle sue applicazioni industriali;**
- **riconoscere gli aspetti di efficacia, efficienza e qualità nella propria attività lavorativa.**

### QUADRO ORARIO

|                        | 1    | 2    | 3    | 4    | 5    |
| ---------------------- | ---- | ---- | ---- | ---- | ---- |
| Insegnamenti generali  | 660  | 660  | 495  | 495  | 495  |
| Insegnamenti indirizzo | 396  | 396  | 561  | 561  | 561  |
| Totale                 | 1056 | 1056 | 1056 | 1056 | 1056 |

## PECUP INDIRIZZO TECNOLOGICO DECLINAZIONE INFORMATICA E TELECOMUNICAZIONI

Il Diplomato in "Informatica e Telecomunicazioni"

- ha competenze specifiche nel campo dei sistemi informatici,dell’elaborazione dell’informazione, delle applicazioni e tecnologie Web, delle reti e degli apparati di comunicazione;
- ha competenze e conoscenze che, a seconda delle diverse articolazioni, si rivolgono all’analisi, progettazione, installazione e gestione di sistemi informatici, basi di dati, reti di sistemi di elaborazione, sistemi multimediali e apparati di trasmissione e ricezione dei segnali;
- ha competenze orientate alla gestione del ciclo di vita delle applicazioni che possono rivolgersi al software: gestionale – orientato ai servizi – per i sistemi dedicati "incorporati";
- collabora nella gestione di progetti, operando nel quadro di normative nazionali e internazionali, concernenti la sicurezza in tutte le sue accezioni e la protezione delle informazioni ("privacy"). 

È in grado di: 
- collaborare, nell’ambito delle normative vigenti, ai fini della sicurezza sul lavoro e della tutela ambientale e di intervenire nel miglioramento della qualità dei prodotti e nell’organizzazione produttiva delle imprese;
- collaborare alla pianificazione delle attività di produzione dei sistemi, dove applica capacità di comunicare e interagire efficacemente, sia nella forma scritta che orale;
- esercitare, in contesti di lavoro caratterizzati prevalentemente da una gestione in team, un approccio razionale, concettuale e analitico, orientato al raggiungimento dell’obiettivo, nell’analisi e nella realizzazione delle soluzioni;
- utilizzare a livello avanzato la lingua inglese per interloquire in un ambito professionale caratterizzato da forte internazionalizzazione;
- definire specifiche tecniche, utilizzare e redigere manuali d’uso. 
 
Nell’indirizzo sono previste le articolazioni "Informatica" e "Telecomunicazioni", nelle quali il profilo viene orientato e declinato.
**In particolare, con riferimento a specifici settori di impiego e nel rispetto delle relative normative tecniche, viene approfondita nell’articolazione "Informatica" l’analisi, la comparazione e la progettazione di dispositivi e strumenti informatici e lo sviluppo delle applicazioni informatiche.**
Nell’articolazione "Telecomunicazioni",viene approfondita l’analisi, la comparazione, la progettazione, installazione e gestione di dispositivi e strumenti elettronici e sistemi di telecomunicazione, lo sviluppo di applicazioni informatiche per reti locali e servizi a distanza. 
A conclusione del percorso quinquennale, il Diplomato nell’indirizzo "Informatica e Telecomunicazioni" consegue i risultati di apprendimento di seguito specificati in termini di competenze.

1. Scegliere dispositivi e strumenti in base alle loro caratteristiche funzionali
2. Descrivere e comparare il funzionamento di dispositivi e strumenti elettronici e di telecomunicazione.
3. Gestire progetti secondo le procedure e gli standard previsti dai sistemi aziendali di gestione della qualità e della sicurezza.
4. Gestire processi produttivi correlati a funzioni aziendali.
5. Configurare, installare e gestire sistemi di elaborazione dati e reti.
6. Sviluppare applicazioni informatiche per reti locali o servizi a distanza. 

In relazione alle articolazioni ”Informatica” e “Telecomunicazioni”, le competenze di cui sopra sono differentemente sviluppate e opportunamente integrate in coerenza con la peculiarità del percorso di riferimento.

## DISCIPLINE A041

### QUADRO ORARIO

|                                                                           | Disciplina                                  | 1            | 2   | 3   | 4   | 5   |
| ------------------------------------------------------------------------- | ------------------------------------------- | ------------ | --- | --- | --- | --- |
| AFM                                                                       | Informatica                                 | 66           | 66  | 66  | 66  |     |
| SIA                                                                       | Informatica                                 | 66           | 66  | 132 | 165 | 165 |
| Relazioni internazionali per il marketing                                 | Informatica                                 | 66           | 66  |     |     |     |
| Relazioni internazionali per il marketing                                 | Tecnologie della comunicazione              |              |     | 66  | 66  |     |
| Turismo                                                                   | Informatica                                 | 66           | 66  |     |     |     |
| Settore tecnologico                                                       | Tecnologie informatiche                     | 99* (66 ITP) |     |     |     |     |
| Settore tecnologico - INFORMATICA e TELECOMUNICAZIONI                     | Scienze e tecnologie applicate              |              | 99  |     |     |     |
| Settore tecnologico - INFORMATICA e TELECOMUNICAZIONI                     | Sistemi e reti                              |              |     | 132 | 132 | 132 |
| Settore tecnologico - INFORMATICA e TELECOMUNICAZIONI                     | TPSIT                                       |              |     | 99  | 99  | 132 |
| Settore tecnologico - INFORMATICA e TELECOMUNICAZIONI                     | Gestione progetto, organizzazione d'impresa |              |     |     |     | 99  |
| Settore tecnologico - INFORMATICA e TELECOMUNICAZIONI - INFORMATICA       | Informatica                                 |              |     | 198 | 198 | 198 |
| Settore tecnologico - INFORMATICA e TELECOMUNICAZIONI - TELECOMUNICAZIONI | Informatica                                 |              | 99  | 99  |     |     |

### INFORMATICA - PRIMO BIENNIO ECONOMICO/TURISMO

l docente di "Informatica" concorre a far conseguire allo studente, al termine del percorso quinquennale, risultati di apprendimento che lo mettono in grado di: utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; individuare ed utilizzare le moderne forme di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete; padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio; agire nel sistema informativo dell’azienda e contribuire sia alla sua innovazione sia al suo adeguamento organizzativo e tecnologico; elaborare, interpretare e rappresentare efficacemente dati aziendali con il ricorso a strumenti informatici e software gestionali; analizzare, con l’ausilio di strumenti matematici e informatici, i fenomeni economici e sociali. 

**Competenze**

- individuare le strategie appropriate per la soluzione di problemi
- utilizzare e produrre testi multimediali
- analizzare dati e interpretarli sviluppando deduzioni e ragionamenti sugli stessi anche con l’ausilio di rappresentazioni grafiche, usando consapevolmente gli strumenti di calcolo e le potenzialità offerte da applicazioni specifiche di tipo informatico
- essere consapevole delle potenzialità e dei limiti delle tecnologie nel contesto culturale e sociale in cui vengono applicate

**Conoscenze**

- Sistemi informatici.
- Informazioni, dati e loro codifica.
- Architettura e componenti di un computer.
- Comunicazione uomo-macchina.
- Struttura e funzioni di un sistema operativo. 
- Software di utilità e software gestionali.
- Fasi risolutive di un problema, algoritmi e loro rappresentazione. 
- Organizzazione logica dei dati.
- Fondamenti di programmazione e sviluppo di semplici programmi in un linguaggio a scelta.
- Struttura di una rete.
- Funzioni e caratteristiche della rete Internet e della posta elettronica.
- Normativa sulla privacy e sul diritto d’autore. 

**Abilità**

- Riconoscere le caratteristiche logico-funzionali di un computer e il ruolo strumentale svolto nei vari ambiti (calcolo, elaborazione, comunicazione, ecc.).
- Riconoscere e utilizzare le funzioni di base di un sistema operativo.
- Raccogliere, organizzare e rappresentare dati/informazioni sia di tipo testuale che multimediale.
- Analizzare, risolvere problemi e codificarne la soluzione.
- Utilizzare programmi di scrittura, di grafica e il foglio elettronico.
- Utilizzare software gestionali per le attività del settore di studio.
- Utilizzare la rete Internet per ricercare fonti e dati di tipo tecnico-scientifico-economico.
- Utilizzare le reti per attività di comunicazione interpersonale.
- Riconoscere i limiti e i rischi dell’uso della tecnologie con particolare riferimento alla privacy.
- Riconoscere le principali forme di gestione e controllo. dell’informazione e della comunicazione specie nell’ambito tecnico-scientifico-economico

### TECNOLOGIE INFORMATICHE - PRIMO BIENNIO TECNOLOGICO

Il docente di "Tecnologie informatiche" concorre a far conseguire allo studente, al termine del percorso quinquennale, risultati di apprendimento che lo mettono in grado di: utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; utilizzare, in contesti di ricerca applicata, procedure e tecniche per trovare soluzioni innovative e migliorative, in relazione ai campi di propria competenza; utilizzare gli strumenti culturali e metodologici acquisiti per porsi con atteggiamento razionale, critico e responsabile di fronte alla realtà, ai suoi fenomeni e ai suoi problemi, anche ai fini dell’apprendimento permanente.

**Competenze**

- individuare le strategie appropriate per la soluzione di problemi
- analizzare dati e interpretarli sviluppando deduzioni e ragionamenti sugli stessi anche con l’ausilio di rappresentazioni grafiche, usando consapevolmente gli strumenti di calcolo e le potenzialità offerte da applicazioni specifiche di tipo informatico
- essere consapevole delle potenzialità e dei limiti delle tecnologie nel contesto culturale e sociale in cui vengono applicate 

**Conoscenze**

- Informazioni, dati e loro codifica.
- Architettura e componenti di un computer.
- Funzioni di un sistema operativo.
- Software di utilità e software applicativi.
- Concetto di algoritmo.
- Fasi risolutive di un problema e loro rappresentazione.
- Fondamenti di programmazione.
- La rete Internet.
- Funzioni e caratteristiche della rete internet.
- Normativa sulla privacy e diritto d’autore.
 
**Abilità**

- Riconoscere le caratteristiche funzionali di un computer (calcolo, elaborazione, comunicazione).
- Riconoscere e utilizzare le funzioni di base di un sistema operativo.
- Utilizzare applicazioni elementari di scrittura, calcolo e grafica.
- Raccogliere, organizzare e rappresentare informazioni.
- Impostare e risolvere problemi utilizzando un linguaggio di programmazione.
- Utilizzare la rete Internet per ricercare dati e fonti.
- Utilizzare le rete per attività di comunicazione interpersonale.
- Riconoscere i limiti e i rischi dell’uso della rete con particolare riferimento alla tutela della privacy. 

### INFORMATICA - SECONDO BIENNIO AFM

Il docente di "Informatica" concorre a far conseguire allo studente, al termine del percorso quinquennale, i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale: utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio; utilizzare, in contesti di ricerca applicata, procedure e tecniche per trovare soluzioni innovative e migliorative, in relazione ai campi di propria competenza; agire nel sistema informativo dell’azienda e contribuire sia alla sua innovazione sia al suo adeguamento organizzativo e tecnologico; elaborare, interpretare e rappresentare efficacemente dati aziendali con il ricorso a strumenti informatici e software gestionali; individuare ed utilizzare le moderne forme di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete.

**Competenze**

- identificare e applicare le metodologie e le tecniche della gestione per progetti
- interpretare i sistemi aziendali nei loro modelli, processi e flussi informativi con riferimento alle differenti tipologie di imprese
- riconoscere i diversi modelli organizzativi aziendali, documentare le procedure e ricercare soluzioni efficaci rispetto a situazioni date
- gestire il sistema delle rilevazioni aziendali con l’ausilio di programmi di contabilità integrata
- applicare i principi e gli strumenti della programmazione e del controllo di gestione, analizzandone i risultati
- inquadrare l’attività di marketing nel ciclo di vita dell’azienda e realizzare applicazioni con riferimento a specifici contesti e diverse politiche di mercato
- utilizzare i sistemi informativi aziendali e gli strumenti di comunicazione integrata d’impresa, per realizzare attività comunicative con riferimento a differenti contesti 

**Conoscenze**

- Sistema Informativo e sistema informatico
- Funzioni di un Data Base Management System (DBMS)
- Struttura di un Data Base
- Fasi di sviluppo di un ipermedia Linguaggi del Web
- Struttura, usabilità e accessibilità di un sito Web
- Servizi di rete a supporto dell’azienda con particolare riferimento alle attività commerciali
- Software di utilità e software gestionali: manutenzione e adattamenti
- Lessico e terminologia di settore, anche in lingua inglese

**Abilità**

- Rappresentare l’architettura di un sistema informativo aziendale
- Documentare con metodologie standard le fasi di raccolta, archiviazione e utilizzo dei dati
- Realizzare tabelle e relazioni di un Data Base riferiti a tipiche esigenze amministrativo-contabili
- Utilizzare le funzioni di un DBMS per estrapolare informazioni
- Produrre ipermedia integrando e contestualizzando oggetti selezionati da più fonti
- Realizzare pagine Web Individuare le procedure che supportano l’organizzazione di un’azienda
- Scegliere e personalizzare software applicativi in relazione al fabbisogno aziendale
- Individuare gli aspetti tecnologici innovativi per il miglioramento dell’organizzazione aziendale
- Utilizzare lessico e terminologia di settore, anche in lingua inglese

### TECNOLOGIE DELLA COMUNICAZIONE - SECONDO BIENNIO ECONOMICO

La disciplina "Tecnologie della comunicazione" concorre a far conseguire allo studente al termine del percorso quinquennale i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale: individuare ed utilizzare le moderne forme di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete; utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; utilizzare, in contesti di ricerca applicata, procedure e tecniche per trovare soluzioni innovative e migliorative, in relazione ai campi di propria competenza; elaborare, interpretare e rappresentare efficacemente dati aziendali con il ricorso a strumenti informatici e software gestionali.

**Competenze**

- utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare
- analizzare il valore, i limiti e i rischi delle varie soluzioni tecniche per la vita sociale e culturale con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio
- identificare e applicare le metodologie e le tecniche della gestione per progetti
- individuare e utilizzare gli strumenti di comunicazione e di team working più appropriati per intervenire nei contesti organizzativi e professionali di riferimento
- interpretare i sistemi aziendali nei loro modelli, processi e flussi informativi con riferimento alle differenti tipologie di imprese
- gestire il sistema delle rilevazioni aziendali con l’ausilio di programmi di contabilità integrata
- inquadrare l’attività di marketing nel ciclo di vita dell’azienda e realizzare applicazioni con riferimento a specifici contesti e diverse politiche di mercato
- utilizzare i sistemi informativi aziendali e gli strumenti di comunicazione integrata d’impresa, per realizzare attività comunicative con riferimento a differenti contesti 

**Conoscenze**

- Sistema informativo e sistema informatico
- Etica e disciplina giuridica della comunicazione
- Aspetti della comunicazione economico-societaria e d’impresa
- Forme e tecniche di comunicazione
- Evoluzione delle tecnologie di comunicazione
- Servizi di rete a supporto della comunicazione aziendale
- Software di utilità per la rappresentazione sintetico-grafica di dati, per il marketing ecc.
- Funzioni di un Data Base Management System (DBMS)
- Editor per gestire oggetti multimediali e pagine web

**Abilità**

- Riconoscere la tipologia di comunicazione adatta al contesto
- Utilizzare le diverse forme di comunicazione a servizio delle esigenze aziendali
- Individuare la tecnologia più efficace per le diverse tipologie di comunicazione
- Applicare prassi e norme relative alla diffusione della comunicazione
- Integrare oggetti multimediali selezionati da più fonti
- Produrre oggetti multimediali di tipo economico-aziendale rivolti ad ambiti nazionali ed internazionali
- Operare con un DBMS per gestire informazioni
- Usare software di utilità in relazione al fabbisogno aziendale
- Elaborare dati e documenti relativi alle attività di marketing

### INFORMATICA - SECONDO BIENNIO E QUINTO ANNO SIA

Il docente di "Informatica" concorre a far conseguire allo studente, al termine del percorso quinquennale, i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale: utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare; individuare ed utilizzare le moderne forme di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive e agli strumenti tecnici della comunicazione in rete; padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio; agire nel sistema informativo dell’azienda e contribuire sia alla sua innovazione sia al suo adeguamento organizzativo e tecnologico; elaborare, interpretare e rappresentare efficacemente dati aziendali con il ricorso a strumenti informatici e software gestionali; analizzare, con l’ausilio di strumenti matematici e informatici, i fenomeni economici e sociali.

**Competenze**

- utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare
- identificare e applicare le metodologie e le tecniche della gestione per progetti
- redigere relazioni tecniche e documentare le attività individuali e di gruppo relative a situazioni professionali
- interpretare i sistemi aziendali nei loro modelli, processi e flussi informativi con riferimento alle differenti tipologie di imprese
- riconoscere i diversi modelli organizzativi aziendali, documentare le procedure e ricercare soluzioni efficaci rispetto a situazioni date
- gestire il sistema delle rilevazioni aziendali con l’ausilio di programmi di contabilità integrata
- applicare i principi e gli strumenti della programmazione e del controllo di gestione, analizzandone i risultati;
- inquadrare l’attività di marketing nel ciclo di vita dell’azienda e realizzare applicazioni con riferimento a specifici contesti e diverse politiche di mercato
- utilizzare i sistemi informativi aziendali e gli strumenti di comunicazione integrata d’impresa, per realizzare attività comunicative con riferimento a differenti contesti

#### SECONDO BIENNIO

**Conoscenze**

- Linguaggi di programmazione
- Metodologia di sviluppo di software
- Fasi di sviluppo di un progetto software
- Sistema informatico e sistema informativo nei processi aziendali
- Sistema Operativo: caratteristiche generali e linee di sviluppo
- Data Base Management System (DBMS)
- Progettazione di Data Base
- Linguaggio SQL
- Software di utilità per la produzione e gestione di oggetti multimediali
- Progettazione d’ipermedia per la comunicazione aziendale
- Linguaggi e strumenti di implementazione per il Web
- Struttura, usabilità e accessibilità di un sito Web
- Reti di computer e reti di comunicazione
- Data base in rete
- Servizi di rete a supporto dell’azienda
- E-commerce
- Social networking 

**Abilità**

- Esprimere procedimenti risolutivi attraverso algoritmi
- Implementare algoritmi con diversi stili di programmazione e idonei strumenti software
- Produrre la documentazione relativa alle fasi di progetto 
- Progettare e realizzare basi di dati in relazione alle esigenze aziendali
- Individuare gli aspetti tecnologici innovativi per il miglioramento dell’organizzazione aziendale
- Individuare le procedure telematiche che supportano l’organizzazione di un’azienda
- Implementare data base remoti con interfaccia grafica sul web in relazione alle esigenze aziendali
- Progettare ipermedia a supporto della comunicazione aziendale
- Progettare e realizzare pagine Web statiche e dinamiche
- Pubblicare su Internet pagine Web 
- Valutare, scegliere e adattare software applicativi in relazione alle caratteristiche e al fabbisogno aziendale
- Utilizzare le potenzialità di una rete per i fabbisogni aziendali

#### QUINTO ANNO

**Conoscenze**

- Casi di diversa complessità focalizzati su differenti attività aziendali
- Tecniche di sviluppo di progetti per l’integrazione dei processi aziendali
- Reti per l’azienda e per la pubblica amministrazione
- Sicurezza informatica
- Tutela della privacy, della proprietà intellettuale e reati informatici

**Abilità**

- Individuare e utilizzare software di supporto ai processi aziendali
- Collaborare a progetti di integrazione dei processi aziendali (ERP)
- Pubblicare su Internet pagine web
- Riconoscere gli aspetti giuridici connessi all’uso delle reti con particolare attenzione alla sicurezza dei dati
- Organizzare la comunicazione in rete per migliorare i flussi informativi
- Utilizzare le funzionalità di Internet e valutarne gli sviluppi 

### SISTEMI E RETI

La disciplina "Sistemi e reti" concorre a far conseguire allo studente al termine del percorso quinquennale i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale dello studente coerenti con la disciplina: cogliere l’importanza dell’orientamento al risultato, del lavoro per obiettivi e della necessità di assumere responsabilità nel rispetto dell’etica e della deontologia professionale; orientarsi nella normativa che disciplina i processi produttivi del settore di riferimento, con particolare attenzione sia alla sicurezza sui luoghi di vita e di lavoro sia alla tutela dell’ambiente e del territorio; intervenire nelle diverse fasi e livelli del processo produttivo,dall’ideazione alla realizzazione del prodotto, per la parte di propria competenza, utilizzando gli strumenti di progettazione, documentazione e controllo; riconoscere gli aspetti di efficacia, efficienza e qualità nella propria attività lavorativa.

**Competenze**

- configurare, installare e gestire sistemi di elaborazione dati e reti
- scegliere dispositivi e strumenti in base alle loro caratteristiche funzionali
- descrivere e comparare il funzionamento di dispositivi e strumenti elettronici e di telecomunicazione;
- gestire progetti secondo le procedure e gli standard previsti dai sistemi aziendali di gestione della qualità e della sicurezza
- utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento disciplinare
- analizzare il valore, i limiti e i rischi delle varie soluzioni tecniche per la vita sociale e culturale con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio

#### SECONDO BIENNIO

**Conoscenze**

- Struttura, architettura e componenti dei sistemi di elaborazione.
- Organizzazione del software di rete in livelli; modelli standard di riferimento.
- Tipologie e tecnologie delle reti locali e geografiche.
- Protocolli per la comunicazione in rete e analisi degli strati 
- Dispositivi per la realizzazione di reti locali; apparati e sistemi per la connettività ad Internet.
- Dispositivi di instradamento e relativi protocolli; tecniche di gestione dell’indirizzamento di rete.
- Problematiche di instradamento e sistemi di interconnessione nelle reti geografiche.
- Normativa relativa alla sicurezza dei dati
- Tecnologie informatiche per garantire la sicurezza e l’integrità dei dati e dei sistemi.
- Lessico e terminologia tecnica di settore anche in lingua inglese.

**Abilità**

- Individuare la corretta configurazione di un sistema per una data applicazione.
- Identificare i principali dispositivi periferici; selezionare un dispositivo adatto all’ applicazione data.
- Installare, configurare e gestire sistemi operativi garantendone la sicurezza.
- Classificare una rete e i servizi offerti con riferimento agli standard tecnologici.
- Progettare, realizzare, configurare e gestire una rete locale con accesso a Internet.
- Installare e configurare software e dispositivi di rete.
- Utilizzare il lessico e la terminologia tecnica di settore anche in lingua inglese.

#### QUINTO ANNO

**Conoscenze**

- Tecniche di filtraggio del traffico di rete.
- Tecniche crittografiche applicate alla protezione dei sistemi e delle reti.
- Reti private virtuali.
- Modello client/server e distribuito per i servizi di rete.
- Funzionalità e caratteristiche dei principali servizi di rete.
- Strumenti e protocolli per la gestione ed il monitoraggio delle reti.
- Macchine e servizi virtuali, reti per la loro implementazione.

**Abilità**

- Installare, configurare e gestire reti in riferimento alla privatezza, alla sicurezza e all’accesso ai servizi.
- Identificare le caratteristiche di un servizio di rete.
- Selezionare, installare, configurare e gestire un servizio di rete locale o ad accesso pubblico.
- Integrare differenti sistemi operativi in rete.

### TECNOLOGIE E PROGETTAZIONE DI SISTEMI INFORMATICI E DI TELECOMUNICAZIONI

La disciplina "Tecnologie e progettazione di sistemi informatici e di telecomunicazioni" concorre a far conseguire allo studente al termine del percorso quinquennale i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale dello studente: orientarsi nella normativa che disciplina i processi produttivi del settore di riferimento, con particolare attenzione sia alla sicurezza sui luoghi di vita e di lavoro sia alla tutela dell’ambiente e del territorio; intervenire nelle diverse fasi e livelli del processo produttivo,dall’ideazione alla realizzazione del prodotto, per la parte di propria competenza, utilizzando gli strumenti di progettazione, documentazione e controllo; riconoscere gli aspetti di efficacia, efficienza e qualità nella propria attività lavorativa; padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio.

**Competenze**

- sviluppare applicazioni informatiche per reti locali o servizi a distanza;
- scegliere dispositivi e strumenti in base alle loro caratteristiche funzionali;
- gestire progetti secondo le procedure e gli standard previsti dai sistemi aziendali di gestione della qualità e della sicurezza. Gestire processi produttivi correlati a funzioni aziendali; 
- configurare, installare e gestire sistemi di elaborazione dati e reti;
- redigere relazioni tecniche e documentare le attività individuali e di gruppo relative a situazioni professionali.

#### SECONDO BIENNIO

**Abilità**

- Principi di teoria e di codifica dell’informazione.
- Classificazione, struttura e funzionamento generale dei sistemi operativi.
- Struttura e organizzazione di un sistema operativo; politiche di gestione dei processi.
- Classificazione e moduli di gestione delle risorse del sistema operativo.
- Tecniche e tecnologie per la programmazione concorrente e la sincronizzazione dell’accesso a risorse condivise.
- Casi significativi di funzionalità programmabili di un sistema operativo
- Fasi e modelli di gestione di un ciclo di sviluppo.
- Tecniche e strumenti per la gestione delle specifiche e dei requisiti di un progetto.
- Tipologie di rappresentazione e documentazione dei requisiti, dell’architettura dei componenti di un sistema e delle loro relazioni ed interazioni.
- Rappresentazione e documentazione delle scelte progettuali e di implementazione in riferimento a standard di settore.
- Normative di settore nazionale e comunitaria sulla sicurezza e la tutela ambientale.

**Conoscenze**

- Identificare e analizzare gli aspetti funzionali dei principali componenti di un sistema operativo.
- Scegliere il sistema operativo adeguato ad un determinato ambiente di sviluppo.
- Progettare e realizzare applicazioni che interagiscono con le funzionalità dei sistemi operativi.
- Progettare e realizzare applicazioni in modalità concorrente.
- Identificare le fasi di un progetto nel contesto del ciclo di sviluppo.
- Documentare i requisiti e gli aspetti architetturali di un prodotto/servizio, anche in riferimento a standard di settore.
- Applicare le normative di settore sulla sicurezza e la tutela ambientale. 

#### QUINTO ANNO

**Abilità**

- Metodi e tecnologie per la programmazione di rete.
- Protocolli e linguaggi di comunicazione a livello applicativo.
- Tecnologie per la realizzazione di web-service. 

**Conoscenze**

- Realizzare applicazioni per la comunicazione di rete.
- Progettare l’architettura di un prodotto/servizio individuandone le componenti tecnologiche.
- Sviluppare programmi client-server utilizzando protocolli esistenti.
- Progettare semplici protocolli di comunicazione.
- Realizzare semplici applicazioni orientate ai servizi.

### GESTIONE PROGETTO, ORGANIZZAZIONE DI IMPRESA

La disciplina "Gestione progetto, organizzazione di impresa" concorre a far conseguire allo studente al termine del percorso quinquennale i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale dello studente: orientarsi nella normativa che disciplina i processi produttivi del settore di riferimento, con particolare attenzione sia alla sicurezza sui luoghi di vita e di lavoro sia alla tutela dell’ambiente e del territorio; riconoscere gli aspetti di efficacia, efficienza e qualità nella propria attività lavorativa; padroneggiare l’uso di strumenti tecnologici con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio; riconoscere e applicare i principi dell’organizzazione, della gestione e del controllo dei diversi processi produttivi.

**Competenze**

- identificare e applicare le metodologie e le tecniche della gestione per progetti
- gestire progetti secondo le procedure e gli standard previsti dai sistemi aziendali di gestione della qualità e della sicurezza
- utilizzare i principali concetti relativi all'economia e all'organizzazione dei processi produttivi e dei servizi
- analizzare il valore, i limiti e i rischi delle varie soluzioni tecniche per la vita sociale e culturale con particolare attenzione alla sicurezza nei luoghi di vita e di lavoro, alla tutela della persona, dell’ambiente e del territorio
- utilizzare e produrre strumenti di comunicazione visiva e multimediale, anche con riferimento alle strategie espressive ed agli strumenti tecnici della comunicazione in rete
- utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca ed approfondimento disciplinare
- redigere relazioni tecniche e documentare le attività individuali e di gruppo relative a situazioni professionali

**Abilità**

- Tecniche e per la pianificazione, previsione e controllo di costi, risorse e software per lo sviluppo di un progetto.
- Manualistica e strumenti per la generazione della documentazione di un progetto
- Tecniche e metodologie di testing a livello di singolo componente e di sistema.
- Norme e di standard settoriali di per la verifica e la validazione del risultato di un progetto.
- Normativa internazionale, comunitaria e nazionale di settore relativa alla sicurezza e alla prevenzione degli infortuni.
- Elementi di economia e di organizzazione di impresa con particolare riferimento al settore ICT.
- Processi aziendali generali e specifici del settore ICT, modelli di rappresentazione dei processi e delle loro interazioni e figure professionali.
- Ciclo di vita di un prodotto/servizio.
- Metodologie certificate per l’assicurazione della qualità di progettazione, realizzazione ed erogazione di prodotti/servizi.

**Conoscenze**

- Gestire le specifiche, la pianificazione e lo stato di avanzamento di un progetto del settore ICT, anche mediante l’utilizzo di strumenti software specifici.
- Individuare e selezionare le risorse e gli strumenti operativi per lo sviluppo di un progetto anche in riferimento ai costi.
- Realizzare la documentazione tecnica, utente ed organizzativa di un progetto, anche in riferimento alle norme ed agli standard di settore.
- Verificare e validare la rispondenza del risultato di un progetto alle specifiche, anche attraverso metodologie di testing conformi ai normative o standard di settore.
- Individuare le cause di rischio connesse alla sicurezza negli ambienti di lavoro.
- Analizzare e rappresentare, anche graficamente, l’organizzazione dei processi produttivi e gestionali delle aziende di settore.
- Comprendere e rappresentare le interdipendenze tra i processi aziendali.
- Applicare le norme e le metodologie relative alle certificazioni di qualità di prodotto e/o di processo.

### INFORMATICA

La disciplina "Informatica" concorre a far conseguire allo studente al termine del percorso quinquennale i seguenti risultati di apprendimento relativi al profilo educativo, culturale e professionale dello studente: utilizzare, in contesti di ricerca applicata, procedure e tecniche per trovare soluzioni innovative e migliorative, in relazione ai campi di propria competenza; cogliere l’importanza dell’orientamento al risultato, del lavoro per obiettivi e della necessità di assumere responsabilità nel rispetto dell’etica e della deontologia professionale; orientarsi nella normativa che disciplina i processi produttivi del settore di riferimento, con particolare attenzione sia alla sicurezza sui luoghi di vita e di lavoro sia alla tuteladell’ambiente e del territorio; intervenire nelle diverse fasi e livelli del processo produttivo, dall’ideazione alla realizzazione del prodotto, per laparte di propria competenza, utilizzando gli strumenti di progettazione, documentazione e controllo; riconoscere gli aspetti di efficacia, efficienza e qualità nella propria attività lavorativa; utilizzare modelli appropriati per investigare su fenomeni e interpretare dati sperimentali; utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca ed approfondimento disciplinare. 

**Competenze**

- utilizzare le strategie del pensiero razionale negli aspetti dialettici ed algoritmici per affrontare situazioni problematiche elaborando opportune soluzioni;
- sviluppare applicazioni informatiche per reti locali o servizi a distanza;
- scegliere dispositivi e strumenti in base alle loro caratteristiche funzionali;
- gestire progetti secondo le procedure e gli standard previsti dai sistemi aziendali di gestione della qualità e della sicurezza;
- redigere relazioni tecniche e documentare le attività individuali e di gruppo relative a situazioni professionali.

#### SECONDO BIENNIO

**Conoscenze**

- Relazioni fondamentali tra macchine, problemi, informazioni e linguaggi.
- Linguaggi e macchine a vari livelli di astrazione.
- Paradigmi di programmazione. Logica iterativa e ricorsiva.
- Principali strutture dati e loro implementazione.
- File di testo.
- Teoria della complessità algoritmica.
- Programmazione ad oggetti.
- Programmazione guidata dagli eventi e interfacce grafiche.
- Strumenti per lo sviluppo del software e supporti per la robustezza dei programmi.
- Linguaggi per la definizione delle pagine web.
- Linguaggio di programmazione lato client per la gestione locale di eventi in pagine web.
- Lessico e terminologia tecnica di settore anche in lingua inglese.
- Normative di settore nazionale e comunitaria sulla sicurezza. 

**Abilità**

- Progettare e implementare algoritmi utilizzando diverse strutture di dati.
- Analizzare e confrontare algoritmi diversi per la soluzione dello stesso problema.
- Scegliere il tipo di organizzazione dei dati più adatto a gestire le informazioni in una situazione data.
- Gestire file di testo.
- Progettare e implementare applicazioni secondo il paradigma ad oggetti.
- Progettare e realizzare interfacce utente.
- Progettare, e realizzare e gestire pagine web statiche con interazione locale.
- Utilizzare il lessico e la terminologia tecnica di settore anche in lingua inglese.
- Applicare le normative di settore sulla sicurezza. 


#### QUINTO ANNO

**Conoscenze**

- Modello concettuale, logico e fisico di una base di dati.
- Linguaggi e tecniche per l'interrogazione e la manipolazione delle basi di dati.
- Linguaggi per la programmazione lato server a livello applicativo.
- Tecniche per la realizzazione di pagine web dinamiche. 

**Abilità**

- Progettare e realizzare applicazioni informatiche con basi di dati.
- Sviluppare applicazioni web-based integrando anche basi di dati.