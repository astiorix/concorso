# Programmi

## Raccomandazioni europee

### Raccomandazione europea 2006 - Competenze chiave

Raccomandazione del parlamento europeo e del consiglio del 18 Dicembre 2006 relativa a competenze chiave per l'apprendimento permanente

- Definizione di competenze chiave per l'apprendimento permanente

Competenze chiave
> Le competenze chiave sono quelle di cui tutti hanno bisogno per la realizzazione e lo sviluppo personali, la cittadinanza attiva, l’inclusione sociale e l’occupazione.

1. comunicazione nella madrelingua;
2. comunicazione nelle lingue straniere;
3. competenza matematica e competenze di base in scienza e tecnologia;
4. competenza digitale;
5. imparare a imparare;
6. competenze sociali e civiche;
7. spirito di iniziativa e imprenditorialità; 
8. consapevolezza ed espressione culturale.

### Raccomandazione europea 2008 - Apprendimento permanente

Raccomandazione del parlamento europeo e del consiglio del 23 aprile 2008 sulla costituzione del Quadro europeo delle qualifiche per l’apprendimento permanente

- Costituzione del Quadro europeo delle qualifiche per l’apprendimento permanente;
- Relative definizioni di conoscenza, capacità e competenza.

Conoscenze 
> Risultato dell'assimilazione di informazioni attraverso l'apprendimento. Le conoscenze sono un insiemedi fatti, principi, teorie e pratiche relative ad un settore di lavoro o di studio. Nel contesto del Quadro europeo delle qualifiche le conoscenze sono descritte come teoriche e/o pratiche;

Abilità
> Indicano le capacità di applicare conoscenze e di utilizzare know-how per portare a termine compiti e risol-vere problemi. Nel contesto del Quadro europeo delle qualifiche le abilità sono descritte come cognitive (comprendenti l'uso del pensiero logico, intuitivo e creativo) o pratiche (comprendenti l'abilità manuale e l'uso di metodi, materiali, strumenti);

Competenze
> Comprovata capacità di utilizzare conoscenze, abilità e capacità personali, sociali e/o metodologiche, insituazioni di lavoro o di studio e nello sviluppo professionale e personale. Nel contesto del Quadro europeo delle quali-fiche le competenze sono descritte in termini di responsabilità e autonomia.

I livelli di queste vanno da 1 ad 8. Il livello 5 corrisponde alla fine del ciclo breve per l'istruzione superiore, mentre l'8 corrisponde al conseguimento del terzo ciclo nel Quadro dei cicli accademici dell'area europea dell'Istruzione superiore (Dottorato)

### Raccomandazione europea 2018 - Competenze chiave

Raccomandazione del consiglio europeo del 22 maggio 2018 relativa alle competenze chiave per l’apprendimento permanenteLe competenze sono definite in questa sede alla stregua di una combinazione di conoscenze, abilità e attitudini appropriate al contesto. 

Competenze chiave aggiornate

1. competenza alfabetica funzionale,
2. competenza multilinguistica,
3. competenza matematica e competenza in scienze, tecnologie e ingegneria,
4. competenza digitale,
5. competenza personale, sociale e capacità di imparare a imparare,
6. competenza in materia di cittadinanza,
7. competenza imprenditoriale,
8. competenza in materia di consapevolezza ed espressione culturali

Competenza alfabetica funzionale
> Capacità di individuare, comprendere, esprimere, creare e interpretare con­cetti, sentimenti, fatti e opinioni, in forma sia orale sia scritta, utilizzando materiali visivi, sonori e digitali attingendo a varie discipline e contesti. Essa implica l’abilità di comunicare e relazionarsi efficacemente con gli altri in modo oppor­tuno e creativo.

Competenza multilinguistica
> Capacità di utilizzare diverse lingue in modo appropriato ed efficace allo scopo di comuni­care. In linea di massima essa condivide le abilità principali con la competenza alfabetica

Competenza matematica e competenza in scienze, tecnologie e ingegneria
> Capacità di sviluppare e applicare il pensiero e la comprensione matematici per risol­vere una serie di problemi in situazioni quotidiane. Capacità di spiegare il mondo che ci circonda usando l’insieme delle cono­scenze e delle metodologie, comprese l’osservazione e la sperimentazione, per identificare le problematiche e trarre conclusioni che siano basate su fatti empirici, e alla disponibilità a farlo.

Competenza digitale
> La competenza digitale presuppone l’interesse per le tecnologie digitali e il loro utilizzo con dimestichezza e spirito critico e responsabile per apprendere, lavorare e partecipare alla società. Essa comprende l’alfabetizzazione informatica e digitale, la comunicazione e la collaborazione, l’alfabetizzazione mediatica, la creazione di contenuti digitali (inclusa la programmazione), la sicurezza (compreso l’essere a proprio agio nel mondo digitale e possedere competenze relative alla cibersicurezza), le questioni legate alla proprietà intellettuale, la risoluzione di problemi e il pensiero critico.

Competenza personale, sociale e capacità di imparare ad imparare
> La competenza personale, sociale e la capacità di imparare a imparare consiste nella capacità di riflettere su sé stessi, di gestire efficacemente il tempo e le informazioni, di lavorare con gli altri in maniera costruttiva, di mantenersi resilienti e di gestire il proprio apprendimento e la propria carriera.

Competenza in maniera di cittadinanza
> Capacità di agire da cittadini responsabili e di partecipare piena­mente alla vita civica e sociale, in base alla comprensione delle strutture e dei concetti sociali, economici, giuridici e politici oltre che dell’evoluzione a livello globale e della sostenibilità

Competenza imprenditoriale
> Capacità di agire sulla base di idee e opportunità e di trasformarle in valori per gli altri. 

Competenza in materia di consapevolezza ed espressioni culturali
> La competenza in materia di consapevolezza ed espressione culturali implica la comprensione e il rispetto di come le idee e i significati vengono espressi creativamente e comunicati in diverse culture e tramite tutta una serie di arti e altre forme culturali. Presuppone l’impegno di capire, sviluppare ed esprimere le proprie idee e il senso della propria funzione o del proprio ruolo nella società in una serie di modi e contesti.

# Linee guida Istituti

## PECuP

Profilo Educativo, Culturale e Professionale dello studente. Art. 1, comma 5, allegato A al D.Lgs. 226/2005

| [Licei](licei.md)                                              | [Istituti Tecnici](istitutiTecnici.md)                                                          | Istituti Professionali                                                                                      |
| -------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| PECuP Licei d.P.R. 15 Marzo 2010, n.89                         | PECuP Istituti Tecnici d.P.R. 15 Marzo 2010, n.88                                               | PECuP Istituti Professionali D.Lgs. 13 Aprile 2017, n. 61                                                   |
| INDICAZIONI NAZIONALI **Obiettivi specifici di apprendimento** | LINEE GUIDA **Profili di uscita e Risultati di apprendimento comuni e specifici per indirizzo** | REGOLAMENTO **Profili di uscita e Risultati di apprendimento comuni e specifici per indirizzo** LINEE GUIDA |

# Licei

## Liceo scientifico opzione delle scienze applicate

### Linee generali e competenze

L’insegnamento di informatica deve contemperare diversi obiettivi: comprendere i principali fondamenti teorici delle scienze dell’informazione, acquisire la padronanza di strumenti dell’informatica, utilizzare tali strumenti per la soluzione di problemi significativi in generale, ma in particolare connessi allo studio delle altre discipline, acquisire la consapevolezza dei vantaggi e dei limiti dell’uso degli strumenti e dei metodi informatici e delle conseguenze sociali e culturali di tale uso. Questi obiettivi si riferiscono ad aspetti fortemente connessi fra di loro, che vanno quindi trattati in modo integrato. Il rapporto fra teoria e pratica va mantenuto su di un piano paritario e i due aspetti vanno strettamente integrati evitando sviluppi paralleli incompatibili con i limiti del tempo a disposizione. Al termine del percorso liceale lo studente padroneggia i più comuni strumenti software per il calcolo, la ricerca e la comunicazione in rete, la comunicazione multimediale, l'acquisizione e l'organizzazione dei dati, applicandoli in una vasta gamma di situazioni, ma soprattutto nell'indagine scientifica, e scegliendo di volta in volta lo strumento più adatto. Ha una sufficiente padronanza di uno o più linguaggi per sviluppare applicazioni semplici, ma significative, di calcolo in ambito scientifico. Comprende la struttura logico-funzionale della struttura fisica e del software di un computer e di reti locali, tale da consentirgli la scelta dei componenti più adatti alle diverse situazioni e le loro configurazioni, la valutazione delle prestazioni, il mantenimento dell'efficienza. L'uso di strumenti e la creazione di applicazioni deve essere accompagnata non solo da una conoscenza adeguata delle funzioni e della sintassi, ma da un sistematico collegamento con i concetti teorici ad essi sottostanti. Il collegamento con le discipline scientifiche, ma anche con la filosofia e l'italiano, deve permettere di riflettere sui fondamenti teorici dell'informatica e delle sue connessioni con la logica, sul modo in cui l'informatica influisce sui metodi delle scienze e delle tecnologie, e su come permette la nascita di nuove scienze. E’ opportuno coinvolgere gli studenti degli ultimi due anni in percorsi di approfondimento anche mirati al proseguimento degli studi universitari e di formazione superiore. In questo contesto è auspicabile trovare un raccordo con altri insegnamenti, in particolare con matematica, fisica e scienze, e sinergie con il territorio, aprendo collaborazioni con università, enti di ricerca, musei della scienza e mondo del lavoro. Dal punto di vista dei contenuti il percorso ruoterà intorno alle seguenti aree tematiche: architettura dei computer (AC), sistemi operativi (SO), algoritmi e linguaggi di programmazione (AL), elaborazione digitale dei documenti (DE), reti di computer (RC), struttura di Internet e servizi (IS), computazione, calcolo numerico e simulazione (CS), basi di dati (BD). 

### Obiettivi specifici di apprendimento - PRIMO BIENNIO

Nel primo biennio sono usati gli strumenti di lavoro più comuni del computer insieme ai concetti di base ad essi connessi. Lo studente è introdotto alle caratteristiche architetturali di un computer: i concetti di hardware e software, una introduzione alla codifica binaria presenta i codici ASCII e Unicode, gli elementi funzionali della macchina di Von Neumann: CPU, memoria, dischi, bus e le principali periferiche. (AC) Conosce il concetto di sistema operativo, le sue funzionalità di base e le caratteristiche dei sistemi operativi più comuni; il concetto di processo come programma in esecuzione, il meccanismo base della gestione della memoria e le principali funzionalità dei file system. (SO) Lo studente conosce gli elementi costitutivi di un documento elettronico e i principali strumenti di produzione. Occorre partire da quanto gli studenti hanno già acquisito nella scuola di base per far loro raggiungere la padronanza di tali strumenti, con particolare attenzione al foglio elettronico. (DE) Apprende la struttura e i servizi di Internet. Insieme alle altre discipline si condurranno gli studenti a un uso efficace della comunicazione e della ricerca di informazioni, e alla consapevolezza delle problematiche e delle regole di tale uso. Lo studente è introdotto ai principi alla base dei linguaggi di programmazione e gli sono illustrate le principali tipologie di linguaggi e il concetto di algoritmo. Sviluppa la capacità di implementare un algoritmo in pseudo-codice o in un particolare linguaggio di programmazione, di cui si introdurrà la sintassi.(AL)

### Obiettivi specifici di apprendimento - SECONDO BIENNIO

Nel secondo biennio si procede ad un allargamento della padronanza di alcuni strumenti e un approfondimento dei loro fondamenti concettuali. La scelta dei temi dipende dal contesto e dai rapporti che si stabiliscono fra l’informatica e le altre discipline. Sarà possibile disegnare un percorso all'interno delle seguenti tematiche: strumenti avanzati di produzione dei documenti elettronici, linguaggi di markup (XML etc), formati non testuali (bitmap, vettoriale, formati di compressione), font tipografici, progettazione web (DE); introduzione al modello relazionale dei dati, ai linguaggi di interrogazione e manipolazione dei dati (BS); implementazione di un linguaggio di programmazione, metodologie di programmazione, sintassi di un linguaggio orientato agli oggetti (AL).

### Obiettivi specifici di apprendimento - QUINTO ANNO

E’ opportuno che l'insegnante - che valuterà di volta in volta il percorso didattico più adeguato alla singola classe - realizzi percorsi di approfondimento, auspicabilmente in raccordo con le altre discipline. Sono studiati i principali algoritmi del calcolo numerico (CS), introdotti i principi teorici della computazione (CS) e affrontate le tematiche relative alle reti di computer, ai protocolli di rete, alla struttura di internet e dei servizi di rete (RC) (IS). Con l'ausilio degli strumenti acquisiti nel corso dei bienni precedenti, sono inoltre sviluppate semplici simulazioni come supporto alla ricerca scientifica (studio quantitativo di una teoria, confronto di un modello con i dati…) in alcuni esempi, possibilmente connessi agli argomenti studiati in fisica o in scienze (CS). 