# LICEO SCIENTIFICO DELLE SCIENZE APPLICATE

## INFORMAZIONI GENERALI

I percorsi liceali forniscono allo studente gli strumenti culturali e metodologici per una comprensione approfondita della realtà, affinché egli si ponga, con atteggiamento razionale, creativo, progettuale e critico, di fronte alle situazioni, ai fenomeni e ai problemi, ed acquisisca conoscenze, abilità e competenze sia adeguate al proseguimento degli studi di ordine superiore, all’inserimento nella vita sociale e nel mondo del lavoro, sia coerenti con le capacità e le scelte personali.

Per raggiungere questi risultati occorre il concorso e la piena valorizzazione di tutti gli aspetti del lavoro scolastico:

- lo studio delle discipline in una prospettiva sistematica, storica e critica;
- la pratica dei metodi di indagine propri dei diversi ambiti disciplinari;
- l’esercizio di lettura, analisi, traduzione di testi letterari, filosofici, storici, scientifici, saggistici e di interpretazione di opere d’arte;
- l’uso costante del laboratorio per l’insegnamento delle discipline scientifiche;
- la pratica dell’argomentazione e del confronto;
- la cura di una modalità espositiva scritta ed orale corretta, pertinente, efficace e personale;
- l‘uso degli strumenti multimediali a supporto dello studio e della ricerca.

## RISULTATI DI APPRENDIMENTO DI TUTTI I PERCORSI LICEALI

A conclusione dei percorsi di ogni liceo gli studenti dovranno:

1. Area metodologica
    * Aver acquisito un metodo di studio autonomo e flessibile, che consenta di condurre ricerche e approfondimenti personali e di continuare in modo efficace i successivi studi superiori, naturale prosecuzione dei percorsi liceali, e di potersi aggiornare lungo l’intero arco della propria vita.
    * Essere consapevoli della diversità dei metodi utilizzati dai vari ambiti disciplinari ed essere in grado valutare i criteri di affidabilità dei risultati in essi raggiunti.
    * Saper compiere le necessarie interconnessioni tra i metodi e i contenuti delle singole discipline.
2. Area logico-argomentativa
    * Saper sostenere una propria tesi e saper ascoltare e valutare criticamente le argomentazioni altrui.
    * Acquisire l’abitudine a ragionare con rigore logico, ad identificare i problemi e a individuare possibili soluzioni.
    * Essere in grado di leggere e interpretare criticamente i contenuti delle diverse forme di comunicazione.
3. Area linguistica e comunicativa
    * Padroneggiare pienamente la lingua italiana e in particolare: dominare la scrittura in tutti i suoi aspetti, da quelli elementari (ortografia e morfologia) a quelli più avanzati (sintassi complessa, precisione e ricchezza del lessico, anche letterario e specialistico), modulando tali competenze a seconda dei diversi contesti e scopi comunicativi; saper leggere e comprendere testi complessi di diversa natura, cogliendo le implicazioni e le sfumature di significato proprie di ciascuno di essi, in rapporto con la tipologia e il relativo contesto storico e culturale; curare l’esposizione orale e saperla adeguare ai diversi contesti. 
    * Aver acquisito, in una lingua straniera moderna, strutture, modalità ecompetenze comunicative corrispondenti almeno al Livello B2 del Quadro Comune Europeo di Riferimento.
    * Saper riconoscere i molteplici rapporti e stabilire raffronti tra la lingua italiana e altre lingue moderne e antiche.
    * Saper utilizzare le tecnologie dell’informazione e della comuniazione per studiare, fare ricerca, comunicare.
4. Area storico-umanistica
    * Conoscere i presupposti culturali e la natura delle istituzioni politiche, giuridiche, sociali ed economiche, con riferimento particolare all’Itala e all’Europa, e comprendere i diritti e i doveri che caratterizano l’essere cittadini.
    * Conoscere, con riferimento agli avvenimenti, ai contesti geografici e ai personaggi più importanti, la storia d’Italia inserita nel contesto europeo e internazionale, dall’antichità sino ai giorni nostri.
    * Utilizzare metodi (prospettiva spaziale, relazioni uomo-ambiente, sintesi regionale), concetti (territorio, regione, localizzazione, scala, diffusione spaziale, mobilità, relazione, senso del luogo...) e strumenti (carte geografiche, sistemi informativi geografici, immagini, dati statistici, fonti soggettive) della geografia per la lettura dei processi storici e per l’analisi della società contemporanea.
    * Conoscere gli aspetti fondamentali della cultura e della tradizione letteraria, artistica, filosofica, religiosa italiana ed europea attraverso lo studio delle opere, degli autori e delle correnti di pensiero più significativi e acquisire gli strumenti necessari per confrontarli con altre tradizioni e culture.
    * Essere consapevoli del significato culturale del patrimonio archeologico, architettonico e artistico italiano, della sua importanza come fondamentale risorsa economica, della necessità di preservarlo attraverso gli strumenti della tutela e della conservazione.
    * Collocare il pensiero scientifico, la storia delle sue scoperte e lo sviluppo delle invenzioni tecnologiche nell’ambito più vasto della storia delle idee.
    * Saper fruire delle espressioni creative delle arti e dei mezzi espressivi, compresi lo spettacolo, la musica, le arti visive.
    * Conoscere gli elementi essenziali e distintivi della cultura e della civiltà dei paesi di cui si studiano le lingue. 
5. Area scientifica, matematica e tecnologica 
    * Comprendere il linguaggio formale specifico della matematica, saper utilizzare le procedure tipiche del pensiero matematico, conoscere i contenuti fondamentali delle teorie che sono alla base della descrizione matematica della realtà.
    * Possedere i contenuti fondamentali delle scienze fisiche e delle scienze naturali (chimica, biologia, scienze della terra, astronomia), padroneggiandone le procedure e i metodi di indagine propri, anche per potersi orientare nel campo delle scienze applicate.
    * **Essere in grado di utilizzare criticamente strumenti informatici e telematici nelle attività di studio e di approfondimento; comprendere la valenza metodologica dell’informatica nella formalizzazione e modellizzazione dei processi complessi e nell’individuazione di procedimenti risolutivi.**

## LICEO SCIENTIFICO

Il percorso del liceo scientifico è indirizzato allo studio del nesso tra cultura scientifica e tradizione umanistica. Favorisce l’acquisizione delle conoscenze e dei metodi propri della matematica, della fisica e delle scienze naturali. Guida lo studente ad approfondire e a sviluppare le conoscenze e le abilità e a maturare le competenze necessarie per seguire lo sviluppo della ricerca scientifica e tecnologica e per individuare le interazioni tra le diverse forme del sapere, assicurando la padronanza dei linguaggi, delle tecniche e delle metodologie relative, anche attraverso la pratica laboratoriale.

Gli studenti, a conclusione del percorso di studio, oltre a raggiungere i risultati di apprendimento comuni, dovranno:

- aver acquisito una formazione culturale equilibrata nei due versanti linguistico-storico-filosofico e scientifico; comprendere i nodi fondamentali dello sviluppo del pensiero, anche in dimensione storica, e i nessi tra i metodi di conoscenza propri della matematica e delle scienze sperimentali e quelli propri dell’indagine di tipo umanistico;
- saper cogliere i rapporti tra il pensiero scientifico e la riflessione filosofica;
- comprendere le strutture portanti dei procedimenti argomentativi e dimostrativi della matematica, anche attraverso la padronanza del linguaggio logico-formale; usarle in particolare nell’individuare e risolvere problemi di varia natura;
- saper utilizzare strumenti di calcolo e di rappresentazione per la modellizzazione e la risoluzione di problemi;
- aver raggiunto una conoscenza sicura dei contenuti fondamentali delle scienze fisiche e naturali (chimica, biologia, scienze della terra, astronomia) e, anche attraverso l’uso sistematico del laboratorio, una padronanza dei linguaggi specifici e dei metodi di indagine propri delle scienze sperimentali;
- essere consapevoli delle ragioni che hanno prodotto lo sviluppo scientifico e tecnologico nel tempo, in relazione ai bisogni e alle domande di conoscenza dei diversi contesti, con attenzione critica alle dimensioni tecnico-applicative ed etiche delle conquiste scientifiche, in particolare quelle più recenti;
- saper cogliere la potenzialità delle applicazioni dei risultati scientifici nella vita quotidiana. 

## OPZIONE SCIENZE APPLICATE

Nell’ambito della programmazione regionale dell’offerta formativa, può essere attivata l’opzione "scienze applicate" che fornisce allo studente competenze particolarmente avanzate negli studi afferenti alla cultura scientifico-tecnologica, con particolare riferimento alle scienze matematiche, fisiche, chimiche, biologiche e all’informatica e alle loro applicazioni

Gli studenti, a conclusione del percorso di studio, oltre a raggiungere i risultati di apprendimento comuni, dovranno: 

- **aver appreso concetti, principi e teorie scientifiche anche attraverso esemplificazioni operative di laboratorio;**
- elaborare l’analisi critica dei fenomeni considerati, la riflessione metodologica sulle procedure sperimentali e la ricerca di strategie atte a favorire la scoperta scientifica;
- analizzare le strutture logiche coinvolte ed i modelli utilizzati nella ricerca scientifica;
- **individuare le caratteristiche e l’apporto dei vari linguaggi (storico-naturali, simbolici, matematici, logici, formali, artificiali);**
- **comprendere il ruolo della tecnologia come mediazione fra scienza e vita quotidiana;**
- **saper utilizzare gli strumenti informatici in relazione all’analisi dei dati e alla modellizzazione di specifici problemi scientifici e individuare la funzione dell’informatica nello sviluppo scientifico;**
- saper applicare i metodi delle scienze in diversi ambiti. 

## QUADRO ORARIO

| 1   | 2   | 3   | 4   | 5   |
| --- | --- | --- | --- | --- |
| 891 | 891 | 990 | 990 | 990 |

## INFORMATICA

L’insegnamento di informatica deve contemperare diversi obiettivi: comprendere i principali fondamenti teorici delle scienze dell’informazione, acquisire la padronanza di strumenti dell’informatica, utilizzare tali strumenti per la soluzione di problemi significativi in generale, ma in particolare connessi allo studio delle altre discipline, acquisire la consapevolezza dei vantaggi e dei limiti dell’uso degli strumenti e dei metodi informatici e delle conseguenze sociali e culturali di tale uso. Questi obiettivi si riferiscono ad aspetti fortemente connessi fra di loro, che vanno quindi trattati in modo integrato. Il rapporto fra teoria e pratica va mantenuto su di un piano paritario e i due aspetti vanno strettamente integrati evitando sviluppi paralleli incompatibili con i limiti del tempo a disposizione. Al termine del percorso liceale lo studente padroneggia i più comuni strumenti software per il calcolo, la ricerca e la comunicazione in rete, la comunicazione multimediale, l'acquisizione e l'organizzazione dei dati, applicandoli in una vasta gamma di situazioni, ma soprattutto nell'indagine scientifica, e scegliendo di volta in volta lo strumento più adatto. Ha una sufficiente padronanza di uno o più linguaggi per sviluppare applicazioni semplici, ma significative, di calcolo in ambito scientifico. Comprende la struttura logico-funzionale della struttura fisica e del software di un computer e di reti locali, tale da consentirgli la scelta dei componenti più adatti alle diverse situazioni e le loro configurazioni, la valutazione delle prestazioni, il mantenimento dell'efficienza. L'uso di strumenti e la creazione di applicazioni deve essere accompagnata non solo da una conoscenza adeguata delle funzioni e della sintassi, ma da un sistematico collegamento con i concetti teorici ad essi sottostanti. Il collegamento con le discipline scientifiche, ma anche con la filosofia e l'italiano, deve permettere di riflettere sui fondamenti teorici dell'informatica e delle sue connessioni con la logica, sul modo in cui l'informatica influisce sui metodi delle scienze e delle tecnologie, e su come permette la nascita di nuove scienze. È opportuno coinvolgere gli studenti degli ultimi due anni in percorsi di approfondimento anche mirati al proseguimento degli studi universitari e di formazione superiore. In questo contesto è auspicabile trovare un raccordo con altri insegnamenti, in particolare con matematica, fisica e scienze, e sinergie con il territorio, aprendo collaborazioni con università, enti di ricerca, musei della scienza e mondo del lavoro. Dal punto di vista dei contenuti il percorso ruoterà intorno alle seguenti aree tematiche: architettura dei computer (AC), sistemi operativi (SO), algoritmi e linguaggi di programmazione (AL), elaborazione digitale dei documenti (DE), reti di computer (RC), struttura di Internet e servizi (IS), computazione, calcolo numerico e simulazione (CS), basi di dati (BD).

### QUADRO ORARIO

| 1   | 2   | 3   | 4   | 5   |
| --- | --- | --- | --- | --- |
| 66  | 66  | 66  | 66  | 66  |

### PRIMO BIENNIO

Nel primo biennio sono usati gli strumenti di lavoro più comuni del computer insieme ai concetti di base ad essi connessi. Lo studente è introdotto alle caratteristiche architetturali di un computer: i concetti di hardware e software, una introduzione alla codifica binaria presenta i codici ASCII e Unicode, gli elementi funzionali della macchina di Von Neumann: CPU, memoria, dischi, bus e le principali periferiche. (AC)
Conosce il concetto di sistema operativo, le sue funzionalità di base e le caratteristiche dei sistemi operativi più comuni; il concetto di processo come programma in esecuzione, il meccanismo base della gestione della memoria e le principali funzionalità dei file system. (SO)
Lo studente conosce gli elementi costitutivi di un documento elettronico e i principali strumenti di produzione. Occorre partire da quanto gli studenti hanno già acquisito nella scuola di base per far loro raggiungere la padronanza di tali strumenti, con particolare attenzione al foglio elettronico. (DE)

### SECONDO BIENNIO

Nel secondo biennio si procede ad un allargamento della padronanza di alcuni strumenti e un approfondimento dei loro fondamenti concettuali. La scelta dei temi dipende dal contesto e dai rapporti che si stabiliscono fra l’informatica e le altre discipline. Sarà possibile disegnare un percorso all'interno delle seguenti tematiche: strumenti avanzati di produzione dei documenti elettronici, linguaggi di markup (XML etc), formati non testuali (bitmap, vettoriale, formati di compressione), font tipografici, progettazione web. (DE)
Introduzione al modello relazionale dei dati, ai linguaggi di interrogazione e manipolazione dei dati. (BS)
Implementazione di un linguaggio di programmazione, metodologie di programmazione, sintassi di un linguaggio orientato agli oggetti. (AL) 

### QUINTO ANNO

E’ opportuno che l'insegnante - che valuterà di volta in volta il percorso didattico più adeguato alla singola classe - realizzi percorsi di approfondimento, auspicabilmente in raccordo con le altre discipline. Sono studiati i principali algoritmi del calcolo numerico (CS), introdotti i principi teorici della computazione (CS) e affrontate le tematiche relative alle reti di computer, ai protocolli di rete, alla struttura di internet e dei servizi di rete (RC) (IS). Con l'ausilio degli strumenti acquisiti nel corso dei bienni precedenti, sono inoltre sviluppate semplici simulazioni come supporto alla ricerca scientifica (studio quantitativo di una teoria, confronto di un modello con i dati...) in alcuni esempi, possibilmente connessi agli argomenti studiati in fisica o in scienze (CS). 
