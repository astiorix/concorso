# PREPARAZIONE ORALE CONCORSO STEM 2021 - CLASSE A041

In questo repo sono presenti il materiale e gli appunti per la preparazione del concorso STEM 2021

## NORMATIVA

[Riassunti](./normativa/normativa.md)

[PNSD](./normativa/fonti/PNSD.pdf)

[Mappa concettuale](./normativa/fonti/mappaNormativa.pdf)

[Fonti](./normativa/fonti)

## INDICAZIONI EUROPEE E NAZIONALI SUI PROGRAMMI

[Indicazioni generali](./programmi/programmi.md)

[Licei](./programmi/licei.md)

[Istituti Tecnici](./programmi/istitutiTecnici.md)

[Istituti Professionali](./programmi/istitutiProfessionali.md)

[Fonti](./programmi/fonti)

[Programmazione Fazzini-Mercatini](./programmi/programmazioneFazzini.pdf)

## MISURE E STRUMENTI DISPENSATIVE E COMPENSATIVE

[Lista misure](./misure/misure.md)

## METODOLOGIE

[Metodologie didattiche](./metodologie/metodologie.md)

[Valutazioni](./metodologie/valutazione.md)

## ESEMPIO PRESENTAZIONE

[Raccolta Requisiti in md](./presentazione/RaccoltaRequisiti.md)

[Raccolta Requisiti in pdf](./presentazione/RaccoltaRequisiti.pdf)
