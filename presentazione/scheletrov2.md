---
title: Requisiti software e casi d'uso 
subtitle: Concorso STEM 2021
author: Lorenzo Salladini
date: 28/07/2021
theme: metropolis
---

## Contenuti<!-- omit in toc -->

- **[Contestualizzazione](#contestualizzazione)**
- [Gli obiettivi](#gli-obiettivi)
- [Progettazione](#progettazione)
- [Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)

# Contestualizzazione

## Scuola, classe, disciplina

Istituto

: Istituto Tecnico

Indirizzo

: Informatica e Telecomunicazioni

Articolazione 

: Informatica

Classe

: IV anno

Disciplina
  
: Tecnologie per la progettazione di sistemi informatici e di telecomunicazioni

Periodo

: I quadrimestre

## Modulo di riferimento

**Ingegneria del software**

L'obiettivo è rendere chiaro quali passi sono necessari per lo sviluppo di un software nei tempi e nei modi corretti

## Riferimenti ad altre UDA

Questa UDA si pone successivamente all'introduzione all'Ingegneria del software e ne costituisce la prima fase operativa.

In seguito si spiegheranno le altre fasi del ciclo di vita del software.

## Composizione della classe

- Si tratta di una classe con livelli eterogenei, composta da 17 elementi
- Presente 1 elemento DSA con dislessia evolutiva
- Presente 1 elemento con BES dovuti a svantaggio linguistico

## Didattica dell'inclusione - PDP[^4]

Per gli elementi con DSA ed altri BES viene steso un **Piano didattico personalizzato** che contiene un quadro descrittivo del ragazzo

Il PDP contiene anche le misure dispensative, gli strumenti compensativi, le strategie che si intendono mettere in atto e i criteri e le modalità di verifica e valutazione

Questo viene presentato alla famiglia che lo firma e lo usa per collaborare con la Scuola ed eventualmente con i Servizi esterni


[^4]: L. 170/2010 con integrazione di Dir.Min. 27/12/2012 e C.M. n. 8 del 06/03/2013

## Misure dispensative

- Dispensa dalla lettura ad alta voce
- Dispensa dalla scrittura sotto dettatura e dal ricopiare alla lavagna
- Dispensa da un eccessivo carico di compiti

## Strumenti compensativi

- Utilizzo di computer, tablet, programmi di videoscrittura con correttore ortografico
- Utilizzo del registratore
- Utilizzo di ausili per il calcolo
- Utilizzo di schemi, tabelle, mappe e diagrammi di flusso come supporto durante compiti e verifiche
- Utilizzo di software didattici e compensativi

## Strategie

- Apprendimento collaborativo e lavori di gruppo
- Facilitazioni sui contenuti con uso di schemi, sintesi, immagini
- Uso di strategie e canali multimediali
- Prima di iniziare la nuova lezione spendere qualche minuto a rivedere la lezione precedente, effettuando i dovuti collegamenti
- Didattica laboratoriale
- Rinforzo continuo per aumentare il livello di autostima dell'alunno

## Criteri e modalità di verifica e valutazione

- Programmare e concordare con l'alunno le verifiche
- Valutazione del contenuto e non degli errori ortografici
- Introdurre prove informatizzate
- Consentire tempi più lunghi per l'esecuzione delle prove
- Lettura delle consegne degli esercizi nei compiti in classe
- Valorizzare il processo di apprendimento e non solo il risultato

## Contenuti<!-- omit in toc -->

- ~~[Contestualizzazione](#contestualizzazione)~~
- **[Gli obiettivi](#gli-obiettivi)**
- [Progettazione](#progettazione)
- [Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)

# Gli obiettivi

## Riferimenti principali

- Linee Guida MIUR[^1]
- PTOF 

[^1]: [Direttiva Ministeriale n. 4 del 16/01/2012 - Documento tecnico linee guida Istituti Tecnici](lineeGazzetta.pdf)

## Competenze

- Scegliere dispositivi e strumenti in base alle loro caratteristiche funzionali 
- Gestire progetti secondo le procedure e gli standard previsti dai sistemi aziendali di gestione della qualità e della sicurezza
- Redigere relazioni tecniche e documentare le attività individuali e di gruppo relative a situazioni professionali 

## Conoscenze

- Fasi e modelli di gestione di un ciclo di sviluppo
- Tecniche e strumenti per la gestione delle specifiche e dei requisiti di un progetto
- Tipologie di rappresentazione e documentazione dei requisiti, dell'architettura dei componenti di un sistema e delle loro relazioni ed interazioni


## Abilità

- Identificare le fasi di un progetto nel contesto del ciclo di sviluppo
- Documentare i requisiti e gli aspetti architetturali di un prodotto/servizio, anche in riferimento allo standard di settore

## Gli assi culturali[^2]

Asse scientifico tecnologico:

- Essere consapevole delle potenzialità delle tecnologie rispetto al contesto culturale e sociale a cui vengono applicate

Asse dei linguaggi:

- Padroneggiare gli strumenti espressivi ed argomentativi indispensabili per gestire l'interazione comunicativa verbale in vari contesti

[^2]: [D.M. Pubblica Istruzione n. 139 22/08/2007](https://archivio.pubblica.istruzione.it/normativa/2007/allegati/all1_dm139new.pdf)

## Competenze di base europee[^3]

- Competenza matematica e competenza in scienze, tecnologia ed ingegneria
- Competenza digitale
- Compentenza personale, sociale e capacità di imparare a imparare
- Competenza imprenditoriale

[^3]: Raccomandazione del Consiglio dell'Unione Europea sulle competenze chiave per l'apprendimento permanente del 22/05/2018

## Contenuti<!-- omit in toc -->

- ~~[Contestualizzazione](#contestualizzazione)~~
- ~~[Gli obiettivi](#gli-obiettivi)~~
- **[Progettazione](#progettazione)**
- [Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)

# Progettazione

## Prerequisiti

- Concetto di Ingegneria del Software e sue motivazioni

## Tempi

| Lezione                | Metodi                                                                    | Ore |
| ---------------------- | ------------------------------------------------------------------------- | --- |
| Lezione in classe      | Lezione frontale                                                          | 1   |
| Lezione in laboratorio | Didattica laboratoriale/ Compito di realtà/ Roleplaying/ Lavoro di gruppo | 2   |
| Lezione in classe      | Lezione frontale                                                          | 1   |
| Lezione in laboratorio | Didattica laboratoriale/ Compito di realtà/ Brainstorming                 | 2   |
| Lezione in laboratorio | Verifica informatizzata                                                   | 2   |
| Lezione in classe      | Correzione, autovalutazione e feedback                                    | 1   |

## Lezione frontale

Si privilegia quando la finalità del momento formativo è la trasmissione dei concetti

Le lezioni frontali possono essere impiegate per l'acquisizione delle conoscenze teoriche mediante uno stile di apprendimento basato sui modelli

Utile quando i partecipanti all'attività formativa siano sprovvisti di elementi conoscitivi rispetto al contenuto trattato

## Compito di realtà

Si tratta di una situazione-problema, quanto più possibile vicina al mondo reale, da risolvere utilizzando conoscenze e abilità già acquisite, mettendo in pratica capacità di problem-solving

Rappresenta uno spazio di autonomia e responsabilizzazione dell’allievo. Per essere efficace, il compito deve avere una connessione evidente e diretta con il mondo reale

## Roleplaying

Tecnica didattica trasversale, finalizzata a potenziare la comunicazione in tutte le sue caratteristiche, l'immedesimazione e l'educazione emotiva.

Gli alunni dovranno mettersi nei panni di uno dei personaggi ed essere capaci di cambiare ruolo in modo da acquisire punti di vista diversi.


## Lavoro di gruppo

Metodologia organizzativa che mira a sviluppare la crescita umana e la socializzazione oltre che le conoscenze e le competenze.

I gruppi possono essere di livello, di compito, elettivi o misti. Possono anche essere scelti in autonomia dal gruppo classe.

## Brainstorming

Si fornisce un problema

Ogni componente del gruppo classe è libero di proporre la propria soluzione **senza che l'insegnante esprima giudizi**

Tutte le idee sono raccolte ed analizzate fino ad arrivare ad una soluzione

## Prima lezione

10 minuti
: verifica e ripasso dei prerequisiti

10 minuti
: spiegazione di un esempio reale

20 minuti
: spiegazione dei concetti teorici

10 minuti
: applicazione dei concetti teorici all'esempio reale 

10 minuti
: verifica della comprensione

## Verifica prerequisiti e comprensione

Si privilegia l'uso di strumenti come Kahoot! e tool similari, in modo da stimolare il discente attraverso l'uso di strumenti digitali e l'aspetto ludico

## Esempio reale

Si ipotizza l'uso di un esempio che possa motivare la classe, come lo sviluppo di un sito di ecommerce per la vendita di abbigliamento.

L'idea è quella di scegliere qualcosa che i ragazzi conoscono in quanto utilizzatori e richiamare argomenti di programmazione web, che risulta solitamente più accattivante

## I concetti principali

- Definizione di requisito software
- Classificazione dei requisiti in base al tipo
- Classificazione dei requisiti in base alla priorità

## Esercizi per casa

- Studio sul libro e sul materiale fornito dal docente degli argomenti teorici
- Ipotizzare dei casi reali ai quali applicare la raccolta dei requisiti

## Seconda lezione

10 minuti
: ripasso dei concetti espressi nella precedente lezione

10 minuti
: presentazione delle proposte elaborate a casa e scelta di una di queste per proseguire la lezione

40 minuti
: si divide la classe in due gruppi che possono essere scelti in autonomia. Un gruppo riveste il ruolo del committente di progetto ed illustra bisogni e necessità. L'altro gruppo rappresenta gli sviluppatori che intervistano i committenti.

10 minuti
: indicazioni operative per trasformare le richieste emerse durante il lavoro di gruppo in lista dei requisiti

40 minuti
: in autonomia si cerca di produrre la lista dei requisiti

10 minuti
: correzione a campione e suggerimenti per proseguire con il lavoro

## Esercizi per casa

Terminare e correggere la lista dei requisiti fatta in classe

## Terza lezione

10 minuti
: raccordo con la lezione precedente

10 minuti
: introduzione al linguaggio UML 

20 minuti
: spiegazione dei concetti teorici

10 minuti
: applicazione dei concetti teorici all'esempio reale 

10 minuti
: verifica della comprensione

## I concetti principali

- Definizione di caso d'uso 
- Rappresentazione dei casi d'uso attraverso i diagrammi UML
  + Inclusione
  + Estensione
  + Generalizzazione

## Esercizi per casa

- Studio sul libro e sul materiale fornito dal docente degli argomenti teorici

## Quarta lezione

10 minuti
: raccordo con la lezione precedente 

20 minuti
: brainstorming nel quale si analizzano le liste dei requisiti ultimate dai ragazzi e si estrapola una lista comune sulla quale continuare il compito di realtà

20 minuti
: introduzione a StarUML

20 minuti
: indicazioni operative sul passaggio da lista dei requisiti a diagramma dei casi d'uso, con un esempio tratto dalla lista appena stilata

40 minuti
: lavoro autonomo di traduzione della lista dei requisiti in diagramma dei casi d'uso

10 minuti
: correzione a campione e suggerimenti per proseguire con il lavoro

## La scelta di StarUML

Si è scelto l'uso di StarUML perché si tratta di un software multipiattaforma, è usato da moltissime grandi aziende e rappresenta quindi uno *standard de facto* per la modellazione UML.

Ha inoltre numerose personalizzazioni dell'interfaccia che possono essere d'aiuto a chi ha difficoltà di lettura di natura linguistica.

## Esercizi per casa

Completare il lavoro di traduzione da lista dei requisiti a rappresentazione dei casi d'uso iniziato in classe

## Contenuti<!-- omit in toc -->

- ~~[Contestualizzazione](#contestualizzazione)~~
- ~~[Gli obiettivi](#gli-obiettivi)~~
- ~~[Progettazione](#progettazione)~~
- **[Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)**

# Verifica, autovalutazione e feedback

## Le verifiche

Valutazione iniziale - prognostica
: atta a prevedere le difficoltà che può incontrare un discente in un percorso di apprendimento

Valutazione formativa - diagnostica
: atta ad aiutare il soggetto a prendere coscienza delle proprie lacune e al docente per valutare in itinere l'azione didattica

Valutazione sommativa - certificativa
: atta a certificare il possesso di determinate competenze da parte del soggetto

## Valutazione formativa

Durante tutto il compito di realtà che è stato proposto il docente procede alla valutazione formativa per cogliere tempestivamente le lacune dei discenti e per modificare la propria azione didattica

Gli studenti sono altresì invitati all'autovalutazione

## Griglia valutazione formativa

| INDICATORI                                            | LIVELLI |
| ----------------------------------------------------- | ------- |
| Accoglie le proposte e propone strategie di lavoro    | 1-4     |
| È autonomo nello svolgimento del lavoro               | 1-4     |
| Applica correttamente il problem solving              | 1-4     |
| Collabora con gli altri per un obiettivo comune       | 1-4     |
| Rispetta l'insegnante e i compagni                    | 1-4     |
| Sa offrire il proprio aiuto ai compagni in difficoltà | 1-4     |

1. Raramente
2. Talvolta
3. Frequentemente
4. Sempre

## Scheda autovalutazione

| Descrittori                                        | Poco | Abbastanza | Molto |
| -------------------------------------------------- | ---- | ---------- | ----- |
| Ho compreso con chiarezza il compito richiesto     |      |            |       |
| Ho impostato il lavoro in modo preciso e razionale |      |            |       |
| Ho potuto valorizzare pienamente le mie conoscenze |      |            |       |
| Ho collaborato proficuamente con i miei compagni   |      |            |       |
| Ho concluso il lavoro rispettando le consegne      |      |            |       |
| Ho raggiuto buoni risultati                        |      |            |       |

- Quali sono stati i momenti di maggiore criticità e come li ho risolti?
- Cosa dovrei migliorare
- Suggerimenti per l'insegnante


## Verifica sommativa

La verifica è strutturata in due domande a risposta aperta e due esercizi, il tempo della prova è di 90 minuti

Si tratta di una prova informatizzata sottoposta con Google Moduli

Per le domande a risposta aperta è possibile rispondere direttamente sul modulo, mentre per gli esercizi è richiesto di allegare un file prodotto con i software indicati dal docente (Office/Openoffice per il primo esercizio, StarUML per il secondo)

## Domande

1. Definisci cosa sono i requisiti software [10 punti]
2. Definisci cosa sono i casi d'uso [10 punti]

## Esercizio 1

Si consideri un negozio di dischi che vende esclusivamente online. Un utente, previa registrazione, può effettuare un ordine inviando i dati di pagamento, che viene memorizzato nel sistema e reso consultabile dal reparto ordine che lo evade. Definisci i requisiti del software. [40 punti]

## Esercizio 2

Rappresenta il diagramma UML dei casi d'uso sapendo che per sviluppare il registro elettronico di una scuola sono stati raccolti i seguenti requisiti funzionali: [40 punti]

- registrazione delle valutazioni da parte dei docenti
- registrazione delle assenze da parte dei docenti
- compilazione della valutazione di fine periodo/anno da parte dei docenti
- registrazione degli argomenti delle lezioni dei compiti assegnati da parte dei docenti
- consultazione delle assenze e delle valutazioni da parte dei docenti, dei genitori e degli studenti
- consultazione degli argomenti delle lezioni e dei compiti assegnati da parte dei docenti, dei genitori e degli studenti

## Griglia di valutazione

| INDICATORI | DESCRITTORI | PUNTEGGIO |
| ---------- | ----------- | --------- |
| CONTENUTO  | Assente     | 0         |
|            | Disorganico | .         |
|            | Incompleto  | .         |
|            | Essenziale  | .         |
|            | Discreto    | .         |
|            | Buono       | .         |
|            | Ottimo      | PMAX/2    |

## Griglia di valutazione

| INDICATORI                     | DESCRITTORI              | PUNTEGGIO |
| ------------------------------ | ------------------------ | --------- |
| LINGUAGGIO TECNICO             | Gravemente insufficiente | 0         |
| (padronanza delle metodologie) | Impreciso                | .         |
|                                | Sufficiente              | .         |
|                                | Buono                    | PMAX/4    |

## Griglia di valutazione

| INDICATORI                    | DESCRITTORI (Esposizione) | PUNTEGGIO |
| ----------------------------- | ------------------------- | --------- |
| ORGANIZZAZIONE CONTENUTI      | Difficoltosa              | 0         |
| (padronanza del ragionamento) | Semplice ed essenziale    | .         |
|                               | Discreta                  | .         |
|                               | Organica, rielaborata     | PMAX/4    |

## Didattica dell'inclusione

Per la verifica verranno utilizzate le seguenti misure per l'inclusione:

- Valutazione del contenuto e non degli errori ortografici
- Prova informatizzata
- Tempo più lungo del 30% per l'esecuzione della prova (120 minuti)
- Lettura delle consegne degli esercizi

## Correzione, autovalutazione e feedback

Viene prevista 1 ora dedicata alla correzione della verifica in classe, alla quale segue un momento dedicato all'autovalutazione

In questo momento è anche opportuno raccogliere i feedback degli alunni in modo da poter modificare l'UDA ove necessario e in modo da capire se si necessita di interventi di recupero individuali o di classe
