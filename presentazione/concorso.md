---
title: Reti locali e reti geografiche - architettura fisica
subtitle: Concorso STEM 2021
author: Lorenzo Salladini
date: 28/07/2021
theme: metropolis
---

## Traccia

Il candidato illustri un'attività didattica inerente il seguente argomento: "Reti locali e reti geografiche: architettura fisica" da trattare in un biennio o triennio di un Istituto Professionale.

## Contenuti<!-- omit in toc -->

- **[Contestualizzazione](#contestualizzazione)**
- [Gli obiettivi](#gli-obiettivi)
- [Progettazione](#progettazione)
- [Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)

# Contestualizzazione

## Scuola, classe, disciplina

Istituto

: Istituto Professionale

Settore

: Servizi

Indirizzo

: Enogastronomia e attività alberghiera

Classe

: II anno

Disciplina

: TIC 

Periodo

: II quadrimestre

## Modulo di riferimento

**Reti e sicurezza**

Con particolare riferimento alla prima parte di questo modulo, in modo da illustrare le basi del funzionamento fisico di una rete di elaboratori

## Riferimenti ad altre Attività Didattiche

Questa attività si pone come introduzione propedeutica alla definizione della rete Internet e all'illustrazione del suo funzionamento e della sua utilità

## Composizione della classe

- Si tratta di una classe con livelli eterogenei, composta da 17 elementi
- Presente 1 elemento DSA con dislessia evolutiva
- Presente 1 elemento con BES dovuti a svantaggio linguistico

## Didattica dell'inclusione - PDP[^1]

Per gli elementi con DSA ed altri BES viene steso un **Piano didattico personalizzato** che contiene un quadro descrittivo del ragazzo

Il PDP contiene anche le misure dispensative, gli strumenti compensativi, le strategie che si intendono mettere in atto e i criteri e le modalità di verifica e valutazione

Questo viene presentato alla famiglia che lo firma e lo usa per collaborare con la Scuola ed eventualmente con i Servizi esterni

[^1]: L. 170/2010 con integrazione di Dir.Min. 27/12/2012 e C.M. n. 8 del 06/03/2013

## Misure dispensative

- Dispensa dalla lettura ad alta voce
- Dispensa dalla scrittura sotto dettatura e dal ricopiare alla lavagna
- Dispensa da un eccessivo carico di compiti

## Strumenti compensativi

- Utilizzo di computer, tablet, programmi di videoscrittura con correttore ortografico
- Utilizzo del registratore
- Utilizzo di ausili per il calcolo
- Utilizzo di schemi, tabelle, mappe e diagrammi di flusso come supporto durante compiti e verifiche
- Utilizzo di software didattici e compensativi

## Strategie

- Apprendimento collaborativo e lavori di gruppo
- Facilitazioni sui contenuti con uso di schemi, sintesi, immagini
- Uso di strategie e canali multimediali
- Prima di iniziare la nuova lezione spendere qualche minuto a rivedere la lezione precedente, effettuando i dovuti collegamenti
- Didattica laboratoriale
- Rinforzo continuo per aumentare il livello di autostima dell'alunno

## Criteri e modalità di verifica e valutazione

- Programmare e concordare con l'alunno le verifiche
- Valutazione del contenuto e non degli errori ortografici
- Introdurre prove informatizzate
- Consentire tempi più lunghi per l'esecuzione delle prove o riduzione delle stesse
- Lettura delle consegne degli esercizi nei compiti in classe
- Valorizzare il processo di apprendimento e non solo il risultato

## Contenuti<!-- omit in toc -->

- ~~[Contestualizzazione](#contestualizzazione)~~
- **[Gli obiettivi](#gli-obiettivi)**
- [Progettazione](#progettazione)
- [Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)

# Gli obiettivi

## Competenze di base europee[^2]

- Competenza matematica e competenza in scienze, tecnologia ed ingegneria
- Competenza digitale
- Competenza personale, sociale e capacità di imparare a imparare

[^2]: Raccomandazione del Consiglio dell'Unione Europea sulle competenze chiave per l'apprendimento permanente del 22/05/2018

## Riferimenti principali

- Linee Guida MIUR [^3] [^4]
- PTOF redatto dall'Istituto

[^3]: DM n. 766 del 23 Agosto 2019

[^4]: legge n. 107 del 13 luglio 2015 (La buona scuola)

## Competenza di indirizzo generale

n.8
: utilizzare le reti e gli strumenti informatici nelle attività di studio, ricerca e approfondimento

n.2
: utilizzare il patrimonio lessicale ed espressivo della lingua italiana secondo le esigenze comunicative nei vari contesti 

## Competenze del profilo di indirizzo

n.1
: utilizzare tecniche tradizionali e innovative di lavorazione, di organizzazione, di commercializzazione dei servizi e dei prodotti enogastronomici, ristorativi e di accoglienza turistico-alberghiera, promuovendo le nuove tendenze alimentari ed enogastronomiche 

## Gli assi culturali[^5]

Asse scientifico tecnologico e professionale

Asse dei linguaggi

[^5]: D.M. Pubblica Istruzione n. 139 22/08/2007

## Conoscenze

- struttura di una rete
- comprensione del concetto di protocollo
- utilizzo e cura degli strumenti e delle attrezzature proprie del settore

## Abilità

- utilizzare la rete Internet per ricercare fonti e dati
- utilizzare le reti per attività di comunicazione interpersonale
- riconoscere i limiti e i rischi dell’uso delle tecnologie
- utilizzare in maniera appropriata gli strumenti e le attrezzature professionali, curandone l'efficienza

## Contenuti<!-- omit in toc -->

- ~~[Contestualizzazione](#contestualizzazione)~~
- ~~[Gli obiettivi](#gli-obiettivi)~~
- **[Progettazione](#progettazione)**
- [Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)

# Progettazione

## Prerequisiti

Per comprendere al meglio i concetti trattati è opportuno che il discente possegga alcune nozioni base su:

- informazioni, dati e loro codifica
- architettura e componenti di un computer 

## Tempi

| Fase      | Metodi                         | Ore |
| --------- | ------------------------------ | --- |
| Lezione 1 | Lezione frontale / Jigsaw      | 1   |
| Lezione 2 | Jigsaw / Lezione frontale      | 1   |
| Lezione 3 | Lavoro di gruppo / Roleplaying | 1   |

## I canali di apprendimento

Visivo verbale

: imparare leggendo - uso della lavagna e degli appunti presi in classe

Visivo non verbale

: visual learning - uso di simboli, immagini, mappe concettuali, grafici e diagrammi

Cinestetico

: learning by doing

## Lezione frontale

Si privilegia quando la finalità del momento formativo è la trasmissione dei concetti

Le lezioni frontali possono essere impiegate per l'acquisizione delle conoscenze teoriche mediante uno stile di apprendimento iasato sui modelli

Utile quando i partecipanti all'attività formativa siano sprovvisti di elementi conoscitivi rispetto al contenuto trattato

## Jigsaw

Specifica modalità di cooperative learning, attraverso la quale ogni studente ha una parte di conoscenza

Sviluppo dell'**interdipendenza positiva**

## Roleplaying

Tecnica didattica trasversale, finalizzata a potenziare la comunicazione in tutte le sue caratteristiche, l'immedesimazione e l'educazione emotiva

Gli alunni dovranno mettersi nei panni di uno dei personaggi ed essere capaci di cambiare ruolo in modo da acquisire punti di vista diversi

## Lavoro di gruppo

Metodologia organizzativa che mira a sviluppare la crescita umana e la socializzazione oltre che le conoscenze e le competenze

I gruppi possono essere di livello, di compito, elettivi o misti. Possono anche essere scelti in autonomia dal gruppo classe

## Lezione 1

15 minuti
: verifica e ripasso dei prerequisiti

15 minuti
: spiegazione del concetto di rete e della sua utilità, facendo continuo riferimento al costante uso "inconsapevole" che i discenti fanno di questa tecnologia 

15 minuti
: classificazione delle reti per estensione geografica, avvalendosi di video su YouTube, con relativa spiegazione delle differenti reti 

15 minuti
: verifica della comprensione attraverso domande dal posto e suddivisione della classe in gruppi in modo applicare la metodologia Jigsaw

## Verifica prerequisiti e comprensione

Si privilegia l'uso di strumenti come Kahoot! e tool similari, in modo da stimolare il discente attraverso l'uso di strumenti digitali e l'aspetto ludico

## Esempi reali 

Per far capire l'utilità delle reti e dell'importanza della loro comprensione si può fare riferimento all'uso dello smartphone, che volta per volta si connette a tipologie di rete diverse

Si farà riferimento inoltre agli strumenti che incontreranno durante il percorso professionale, ad esempio palmari per prendere le ordinazioni

Si suggerisce ai ragazzi di provare a casa l'utilizzo dell'app Fing, che analizza la rete in cui si è connessi per capire quanti dispositivi ormai in ogni casa fanno uso delle tecnologie presentate

## I concetti principali

- Definizione di rete come insieme di elaboratori connessi attraverso un sistema di comunicazione, per condividere risorse e scambiarsi informazioni **utilizzando il medesimo protocollo**
- Estensione geografica delle reti: PAN/LAN/MAN/GAN/WAN
- Come risorsa su YouTube si cerca di utilizzare un video che contenga molti input di tipo visivo non verbale, ad esempio [Network Types: LAN, WAN, PAN, CAN, MAN, SAN, WLAN](https://www.youtube.com/watch?v=4_zSIXb7tLQ)

## Esercizi per casa

Studio sul libro e sul materiale fornito dal docente degli argomenti teorici, applicando il metodo Jigsaw. 

Ogni componente del gruppo è **responsabile** dello studio di una tipologia di rete ed ha il compito di recuperare informazioni oltre quelle fornite da docente e libro, in modo da spiegarle poi al resto del gruppo.

## Lezione 2

30 minuti
: finalizzazione del lavoro Jigsaw fatto a casa, facendo esporre quello che i ragazzi hanno studiato ai relativi gruppi

20 minuti
: introduzione del concetto di topologia delle reti con spiegazione delle principali topologie

10 minuti
: verifica della comprensione attraverso veloci domande dal posto e assegnazione dei compiti per casa

## I concetti principali

- Topologie di rete: Reti a bus, Reti ad anello, Reti a stella, Reti a maglie
- Anche in questo caso per la spiegazione ci si può avvalere di risorse video su Youtube come [Network Topologies (Star, Bus, Ring, Mesh, Ad hoc, Infrastructure, & Wireless Mesh Topology)](https://www.youtube.com/watch?v=zbqrNg4C98U)

## Esercizi per casa

Studio sul libro e sul materiale fornito dal docente degli argomenti teorici

## Lezione 3

10 minuti
: richiamo delle nozioni esposte nella precedente lezione, suddivisione della classe in 4 gruppi, uno per ogni topologia spiegata

50 minuti
: lavoro di gruppo riguardante le topologie di rete 

## Lavoro di gruppo e roleplaying

Ad ogni gruppo viene consegnato un rotolo di spago, di modo che possano configurarsi secondo la topologia della rete assegnatagli.

A turno, si fanno prove di comunicazione in modo tale da capire volta per volta quali nodi vengono coinvolti durante le comunicazioni

## Interdisciplinarità

È possibile approfondire anche il concetto di protocollo di comunicazione e per estensione coinvolgere il docente di italiano e quello di prima lingua per effettuare un parallelismo con il concetto dei registri linguistici

## Contenuti<!-- omit in toc -->

- ~~[Contestualizzazione](#contestualizzazione)~~
- ~~[Gli obiettivi](#gli-obiettivi)~~
- ~~[Progettazione](#progettazione)~~
- **[Verifica, autovalutazione e feedback](#verifica-autovalutazione-e-feedback)**

# Verifica, recupero e potenziamento

## Le verifiche

Valutazione iniziale - prognostica
: atta a prevedere le difficoltà che può incontrare un discente in un percorso di apprendimento

Valutazione formativa - diagnostica
: atta ad aiutare il soggetto a prendere coscienza delle proprie lacune e al docente per valutare in itinere l'azione didattica

Valutazione sommativa - certificativa
: atta a certificare il possesso di determinate competenze da parte del soggetto

## Valutazione formativa

Durante i lavori di gruppo che sono stati proposti il docente procede alla valutazione formativa per cogliere tempestivamente le lacune dei discenti e per modificare la propria azione didattica

Gli studenti sono altresì invitati all'autovalutazione

## Griglia valutazione formativa

| INDICATORI                                            | LIVELLI |
| ----------------------------------------------------- | ------- |
| Accoglie le proposte e propone strategie di lavoro    | 1-4     |
| È autonomo nello svolgimento del lavoro               | 1-4     |
| Applica correttamente il problem solving              | 1-4     |
| Collabora con gli altri per un obiettivo comune       | 1-4     |
| Rispetta l'insegnante e i compagni                    | 1-4     |
| Sa offrire il proprio aiuto ai compagni in difficoltà | 1-4     |

1. Raramente
2. Talvolta
3. Frequentemente
4. Sempre

## Scheda autovalutazione

| DESCRITTORI                                        | Livelli |
| -------------------------------------------------- | ------- |
| Ho compreso con chiarezza il compito richiesto     | 1-5     |
| Ho impostato il lavoro in modo preciso e razionale | 1-5     |
| Ho potuto valorizzare pienamente le mie conoscenze | 1-5     |
| Ho collaborato proficuamente con i miei compagni   | 1-5     |
| Ho concluso il lavoro rispettando le consegne      | 1-5     |
| Ho raggiuto buoni risultati                        | 1-5     |

- Quali sono stati i momenti di maggiore criticità e come li ho risolti?
- Cosa dovrei migliorare
- Suggerimenti per l'insegnante


## Verifica sommativa

La verifica sommativa dell'attività didattica sarà svolta durante le verifiche orali che riguardano il modulo di riferimento all'interno della progettazione

## Possibili domande

1. Attraverso quali parametri è possibile classificare le reti?
2. Quali sono le regole di comunicazione in una rete a stella?
3. La rete Internet che estensione geografica ha? A quale topologia è ascrivibile?

## Griglia di valutazione

![Griglia parte 1](griglia1.png)

## Griglia di valutazione

![Griglia parte 2](griglia2.png)

## Didattica dell'inclusione

Per la verifica verranno utilizzate le seguenti misure per l'inclusione:

- Utilizzo di mappe concettuali
- Si dispensa dallo scrivere alla lavagna
- Valorizzazione del processo di apprendimento

## Azioni di recupero e potenziamento

- Le azioni di recupero verranno effettuate in itinere durante le verifiche sui prerequisiti dell'attività didattica successiva
- Se non dovesse bastare potrebbe essere opportuno coinvolgere il tutor responsabile del PFI del discente
- Come potenziamento per i ragazzi particolarmente interessati si suggerisce l'analisi delle reti che lo circondano (scuola, casa) per capirne meglio le caratteristiche