## Misure dispensative e interventi di individualizzazione

- Dispensa dalla lettura ad alta voce
- Dispensa dall’uso dei quattro caratteri di scrittura (scuola primaria
- Dispensa dall’uso del corsivo e dello stampato minuscolo (scuola primaria
- Dispensa dalla scrittura sotto dettatura
- Dispensa dal ricopiare dalla lavagna
- Dispensa dallo studio mnemonico delle tabelline, dei verbi, delle poesie
- Riduzione delle consegne senza modificare gli obiettivi
- Dispensa da un eccessivo carico di compiti con riadattamento e riduzione delle pagine da studiare, senza modificare gli obiettivi
- Dispensa dalla sovrapposizione di compiti e interrogazioni di più materie
- Dispensa parziale dallo studio della lingua straniera in forma scritta
- Concordare modalità e i tempi delle verifiche scritte con possibilità di utilizzare supporti multimediali
- Concordare tempi e modalità delle interrogazioni
- Nelle verifiche, riduzione e adattamento del numero degli esercizi senza modificare gli obiettivi
- Nelle verifiche scritte, utilizzo di domande a risposta multipla e riduzione al minimo delle domande a risposte aperte
- Parziale sostituzione o completamento delle verifiche scritte con prove orali consentendo l’uso di schemi riadattati e/o mappe durante l’interrogazione
- Valutazione dei procedimenti e non dei calcoli nella risoluzione dei problemi
- Valutazione del contenuto e non degli errori ortografici

## Strumenti compensativi

- Utilizzo di computer, tablet, scanner
- Utilizzo di programmi di video-scrittura con correttore ortografico (possibilmente vocale) e con tecnologie di sintesi vocale (anche per le lingue straniere)
- Utilizzo di risorse audio (file audio digitali, audiolibri…).
- Utilizzo del registratore
- Utilizzo di ausili per il calcolo (tavola pitagorica, linee dei numeri…) ed eventualmente della calcolatrice con foglio di calcolo
- Utilizzo di schemi, tabelle, mappe e diagrammi di flusso come supporto durante compiti e verifiche scritte
- Utilizzo di   formulari e di schemi e/o mappe delle varie discipline scientifiche come supporto durante compiti e verifiche scritte
- Utilizzo di mappe e schemi durante le interrogazioni, eventualmente anche su supporto digitalizzato (presentazioni multimediali), per facilitare il recupero delle informazioni
- Utilizzo di dizionari digitali (cd rom, risorse on line)
- Utilizzo di software didattici e compensativi (free e/o commerciali) 

## Strategie utilizzate

- Apprendimento collaborativo e lavori di gruppo
- Tutoraggio tra pari
- Facilitazioni sui contenuti con uso di schemi, sintesi, immagini, ecc.
- Uso di mappe
- Uso di strategie e canali multimediali
- Indicazioni sul metodo di studio (sottolineare parole chiave, note a margine …)
- Sintesi di spiegazione fatta al termine della lezione
- Prima di iniziare la nuova lezione spendere qualche minuto a rivedere la lezione precedente, effettuando i dovuti collegamenti
- Interrogazione programmata
- Didattica laboratoriale
- Consegna anticipata del testo in vista di un’esercitazione
- Consegna anticipata di schemi grafici relativi all’argomento che si andrà a sviluppare
- Dividere gli obiettivi di un compito in sotto obiettivi
- Interrogare riducendo o frammentando la quantità di contenuto 
- Rinforzo continuo per aumentare il livello di autostima dell’alunno

## Criteri e modalità di verifica e valutazione

- Programmare e concordare con l’alunno le verifiche
- Prevedere verifiche orali a compensazione di quelle scritte (soprattutto per la lingua straniera) ove necessario
- Valutazione del contenuto e non degli errori ortografici nei testi e nelle verifiche scritte
- Permettere l’uso di strumenti e mediatori didattici (mappe concettuali, mappe cognitive) sia nelle prove scritte, sia nelle verifiche orali
- Predisporre schemi di sviluppo della consegna con domande guida
- Introdurre prove informatizzate
- Consentire tempi più lunghi per l’esecuzione delle prove
- Ridurre quantitativamente la consegna o adattarla senza modificarne gli obiettivi formativi
- Pianificare prove di valutazione formativa 
- Lettura delle consegne degli esercizi nei compiti in classe
- Valutazione dei procedimenti e non dei calcoli nella risoluzione dei problemi
- Valorizzare il contenuto nell’esposizione orale, tenendo conto di eventuali difficoltà espositive
- Valorizzare il processo di apprendimento dell’allievo e non valutare solo il prodotto/risultato
- Modificare la percentuale soglia di sufficienza delle prove scritte





