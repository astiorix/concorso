# La valutazione

## La docimologia

La scienza che ha per oggetto tutto ciò che è connesso alla misurazoine ed alla valutazione in ambito educativo.

## Tipi di valutazione

Valutazione di sistema
> L'insieme di operazioni che consentono di esprimere un giudizio sul funzionamento di un sistema formativo.

Valutazione di processo
> L'insieme di operazioni condotte in itinere per raccogliere dati sull'andamento di un processo educativo/didattico al fine di migliorare lo stesso processo.

Valutazione di prodotto
> L'insieme di operazioni compiute per accertare il risultato delle attività di apprendimento degli studenti.

## Quando si valuta

Valutazione iniziale - prognostica
> Se l’obiettivo è prevedere quali difficoltà potrà incontrare un discente in un percorso di apprendimento, anche al fine di consigliare un percorso personalizzato, tale valutazione si dice prognostica. Se tale valutazione viene messa in atto prima di iniziare un percorso formativo, essa si dice iniziale.

Valutazione formativa - diagnostica
> Se l’obiettivo è individuare possibili lacune nelle competenze di un soggetto, si parla di valutazione diagnostica. Se questa avviene durante un percorso formativo, ad esempio al termine di una unità didattica, allo scopo di aiutare il soggetto a prendere coscienza delle proprie lacune e consigliare interventi di recupero, si parla di valutazione formativa.

Valutazione sommativa - certificativa
> Se l’obiettivo è certificare il possesso di determinate competenze da parte del soggetto, allora si parla di valutazione certificativa. Se questa avviene al termine di un intervento formativo, tale valutazione si dice sommativa.

## La valutazione

Ogni valutazione implica un confronto tra una situazione attesa e una situazione osservata.
La discrepanza tra le due situazioni, se opportunamente analizzata, può fornire informazioni quali:

- Sapere quali conoscenze/competenze/abilità l'allievo ha assimilato e i conseguenti percorsi di recupero
- Indicazioni importantisime per valutare l'azione didattica

Ci si aspetta che le prove abbiano anche funzione proattiva, cioè fungano da stimolo per la promozione di nuovi apprendimenti.

## Le domande a cui risponde il feedback

**A che punto sono?** Deve rendere consapevole lo studnte del punto in cui trova nel percorso di apprendimento.

**Dove voglio arrivare?** Gli insegnanti devono avere un'idea chiara e renderla chiara anche agli studenti di qual è l'obiettivo di apprendimento successivo.

**Come faccio ad arrivarci?** L'indicazione di ciò che è necessario fare per raggiungere l'obiettivo successivo.

## Chi valuta

Eterovalutazione
> Modello classico in cui l'insegnante valuta l'allievo sulla base della rilevazione di quanto questi "sa" o "non sa", "sa fare" o "non sa fare"

Autovalutazione
> L'allievo confronta i propri prodotti con un insieme di prodotti "tipo" e ne identifica le differenze, oppure viene chiamato a controllare la conformità del proprio prodotto a un insieme di criteri

Valutazione tra pari
> L'allievo fa valutare i propri processi e prodotti ad altri allievi e simmetricamente valuta i loro. Favorisce l'autoriflessione di gruppo e coinvolge attivamente gli studenti nel processo di valutazione

## Come si valuta

### Item di conoscenza

Possedere un corpus di conoscenze relative ad un dato dominio significa ad esempio saperne la terminologia specifica, conoscere le regole, i processi e i metodi per derivare nuove conoscenze da quelle date, gli schemi di classificazione, i criteri di valutazione e di generalizzazione delle informazioni.
Il possesso di conoscenze può essere rilevato attraverso semplici domande a risposta chiusa, inserite in un test di profitto. La risposta esatta è frutto di una semplice abilità mnemonica di tipo riconoscitivo

Item a scelta multipla
> L'allievo deve scegliere la risposta esatta tra un certo numero di alternative (solitamente da 3 a 5). Tutte le risposte proposte (la risposta corretta e i distrattori, ossia le risposte sbagliate) devono essere plausibili in relazione alla domanda. La risposta corretta è una sola.

Item vero/falso
> Chiedono all’allievo di dire se una data affermazione è vera o falsa. L’utilizzo di questi item è sempre sconsigliabile, data l’alta probabilità che il soggetto possa rispondere correttamente per caso (50 su 100). Un possibile correttivo può essere chiedere al soggetto la motivazione della sua scelta.

Item di completamento singoli
> L'allievo deve completare un brano proposto in forma incompleta, scegliendo il completamento esatto tra un elenco di alternative, tutte plausibili

Item di completamento multipli
> L'allievo deve completare un brano in cui sono stati omessi frasi o termini, scegliendoli tra un elenco in cui sono presenti anche distrattori. Le parole o le frasi omesse devono essere specifiche del dominio conoscitivo in oggetto. Questi item vengono anche detti cloze.

Item di corrispondenza
> Propongono all’allievo due elenchi di nomi o concetti appartenenti ad un dominio di conoscenza omogeneo e questi deve metterli in relazione. Opzionalmente è possibile chiedere all’allievo di spiegare la natura della relazione tra i due concetti, oltre che indicarla.

### Item di comprensione

Le abilità di comprensione fanno riferimento alla capacità del soggetto di interpretare le informazioni date per generarne di nuove non fornite in modo esplicito,alla capacità di identificar econtenuti manifesti e latenti di un brano, alla capacità di individuare i punti nodali di un discorso e gli elementi secondari.

Item di comprensione della lettura
> Rilevano l’abilità dell’allievo di cogliere il senso di un brano, il significato contestuale dei termini, i messaggi impliciti ed espliciti, lo sviluppo del discorso. Viene chiesto all’allievo di leggere un brano e rispondere ad un elenco di domande chiuse sul brano stesso.

### Item di analisi
Le abilità di analisi fanno riferimento alla capacità di separare elementi di conoscenza, di evidenziare le relazioni tra di essi e i principi organizzativi generali che consentono di ordinare elementi e relazioni in strutture di conoscenza. Nelle abilità di analisi rientrano anche il riconoscimento di percorsi di ragionamento errati, la valutazione della rilevanza dei dati in proprio possesso per il raggiungimento degli obiettivi che ci si prefigge, il riconoscimento della struttura organizzativa di un lavoro.

Mappe concettuali
> Le abilità di analisi possono essere rilevate chiedendo all’allievo di costruire una mappa concettuale sulla base di uno o più testi che l’allievo deve leggere ed analizzare.

Categorizzazione
> Un indice delle abilità di analisi dell'allievo è la sua capacità di creare categorie complesse a partire da stimoli dati. E' possibile chiedere all'allievo di trovare tutti i raggruppamenti possibili che egli vede tra gli oggetti proposti, per avere un indice di come egli sappia creare ed utilizzare categorie di pensiero.

Schedatura
> Quando sui vari oggetti rileviamo determinate proprietà (uguali per tutti gli oggetti), le quali possono assumere stati diversi da oggetto a oggetto, stiamo operando una schedatura degli oggetti. 

Creazione di tassonomie
> Se tra i concetti sotto esame è possibile definire relazioni gerarchiche, l’operazione produce una tassonomia. In una tassonomia la gerarchia riguarda l’inclusivitàdei concetti: un concetto che si trova più in alto nella gerarchia include quelli che si trovano più in basso.

Individuazione di sequenze e trasformazioni
> Larappresentazionedelle trasformazioni avviene attraverso diagrammi procedurali.

### Item di sintesi

Le abilità di sintesi riguardano la capacità dell’allievo di combinare più informazioni in un quadro unitario e coerente (ad esempio abilità espressive e argomentative), di definire piani e sequenze di operazioni in funzione di determinati obiettivi, di formulare ipotesi su relazioni astratte e deduzioni sulla base dei sistemi di relazioni così definiti. Queste abilità vengono coinvolte nello scrivere una relazione o discorso ben organizzati su un tema specifico, nell’elaborare una storia attraverso la fantasia creativa, nel formulare un progetto, integrare conoscenze provenienti da fonti differenti per elaborare un piano volto a risolvere un problema specifico, formulare uno schema originale per classificare oggetti, eventi, idee.

Valutazioni di performance
> Allo studente viene chiesto di costruire elaborati di varia natura rispettando standard predefiniti. Tra gli elaborati è possibile annoverare relazioni, rapporti di ricerca, progetti, prodotti multimediali a tema, elenchi di possibili domande su un dato dominio conoscitivo e relative risposte. Lo studente dovrà poi difendere il proprio elaborato da critiche mosse dall’esterno (ad esempio dai propri pari), dimostrando la bontà delle sue scelte.

Saggio breve
> La valutazione con saggi brevi prevede domande (dette anche item) a risposta aperta (che possono anche includere problemi ed esercizi), alle quali l’allievo deve rispondere in uno spazio limitato. La correzione non può essere automatizzata, come per i test a risposta chiusa, ma è possibile proporre all’allievo i criteri da utilizzare nella correzione del proprio elaborato e di assegnarsi un punteggio per ogni criterio soddisfatto, guidandolo così ad autovalutarela propria prestazione.

### Item di valutazione

Nelle abilità di valutazione rientra la capacità dell’allievo di esprimere giudizi valutativi su propri elaborati o su elaborati altrui in rapporto a determinati criteri valutativi, scelti dallo studente o proposti dall'insegnante. Nella valutazione l’allievo è chiamato a porre delle critiche alle argomentazioni degli autori, ad esprimere giudizi sulla validità di procedure proposte, a scegliere i prodotti migliori tra un insieme di prodotti dati (es. testi o elaborati di varia natura), sulla base di un insieme di criteri espliciti e condivisi, giustificando e argomentando le ragioni della propria scelta.

## Strumenti per la valutazione

[Google Forms](https://www.google.com/intl/it/forms/about/)

[Kahoot!](https://kahoot.com/)

[QuestBase](http://www.questbase.com/)

[SurveyMonkey](https://it.surveymonkey.com/)

[Mentimeter](https://www.mentimeter.com/)

## Dalla valutazione scolastica "tradizionale" alla valutazione "autentica"

Valutando ciò che un ragazzo sa, si controlla e si verifica la riproduzione, ma non la costruzione della conoscenza e lo sviluppo della competenza e neppure la capacità di applicazione in un contesto reale delle conoscenze e delle abilità possedute.

Nel valutare conoscenze (e abilità) si richiedono spesso prestazioni semplici, basate su acquisizioni di tipo "scolastico"

Nel valutare competenze, si richiedono prestazioni più complesse, basate sulla produzione di soluzioni a problemi tratti dal mondo reale

