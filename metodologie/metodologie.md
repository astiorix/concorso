# METODOLOGIE

Metodologia
>È l'organizzazione delle tecniche, delle procedure e degli strumenti ritenuti idonei a far conseguire gli obiettivi di apprendimento programmati. Le tecniche sono aspetti particolari per la realizzazione complessiva del metodo. Esse richiedono a volte l'uso dei mezzi quali: penna, quaderno lavagna e computer.

Metodo euristico
>Si tratta del metodo della scoperta e consiste nel condurre gradualmente l'alunno a scoprire da solo ciò che si desidera egli conosca mediante un costante e attivo suo coinvolgimento nei percorsi di ricerca e d'interpretazione.

| Architettura                          | Fattori caratterizzanti                                        | Strategia didattica                |
| ------------------------------------- | -------------------------------------------------------------- | ---------------------------------- |
| Recettiva - trasmissiva               | Controllo da parte del docente                                 | Esposizione classica               |
|                                       | Pre-strutturazione dell’informazione                           | Esposizione multimediale           |
|                                       | Interazione assente o scarsa                                   |                                    |
| Comportamentale/Direttivo-interattiva | Controllo da parte del docente                                 | Istruzione sequenziale interattiva |
|                                       | Alta pre-strutturazione dell’informazione                      | Modellamento (apprendistato)       |
|                                       | Interazione continua docente/discente                          | Supporto al comportamento positivo |
|                                       | Importanza del feedback                                        |                                    |
| Simulativa                            | Controllo da parte dell’allievo                                | Studio di caso                     |
|                                       | Pre-strutturazione dell’informazione                           | Simulazione simbolica              |
|                                       | Forte interazione tra allievo e modello/sistema                | Game Based Learning                |
|                                       |                                                                | Role playing/drammatizzazione      |
| Collaborativa                         | Controllo da parte dell’allievo                                | Mutuo insegnamento                 |
|                                       | Minore/maggiore pre-strutturazione degli obiettivi             | Apprendimento cooperativo          |
|                                       | Forte interazione tra pari                                     | Discussione                        |
| Esplorativa                           | Controllo da parte dell’allievo                                | Problem Based Learning             |
|                                       | Scarsa o assente pre-strutturazione dell’informazione          | Metodo per progetti                |
|                                       | Scarsa interazione                                             |                                    |
| Metacognitiva/Autoregolativa          | Trasferimento del controllo da parte del docente all’allievo   | Metacognizione                     |
|                                       | Crescente capacità del discente di organizzare le informazioni | autoregolazione                    |
|                                       | Controllo completo da parte dell’allievo                       |                                    |



## STEM

Science, Technology, Engineering e Math. Non si tratta di una metodologia didattica e neanche di 4 discipline a sè stanti ma di 4 discipline integrate in un nuovo paradigma educativo basato su applicazioni reali ed autentiche.
Viene mostrato agli studenti come il metodo scientifico possa essere applicato alla vita quotidiana. Le STEM consentono di insegnare agli studenti il pensiero computazionale concentrandosi sulle applicazioni del mondo reale in un’ottica di problem solving. Più di recente, inoltre, è sorta anche la necessità di includere la lettura tra le discipline da tutelare, evolvendo quindi da STEM o STEAM in STREAM – con l’aggiunta della R per Reading.

### Ruolo del docente

Monitorare le attività e supportare i ragazzi. Spesso le esperienze laboratoriali prevedono l’utilizzo di materiali non sempre sicuri, diventa necessario che il docente valuti tutti gli aspetti relativi alla sicurezza nel rispetto del TU 81/08. 
Il docente non trasmette i contenuti in modo diretto attraverso una lezione teorica/frontale ma conduce i ragazzi all’acquisizione delle conoscenze attraverso attività sperimentali guidate.
Il docente non corregge gli errori e non interviene durante lo svolgimento delle attività di laboratorio ma guida i ragazzi al superamento delle difficoltà senza fornire le risposte.

### Ruolo dello studente

Lo Studente, attraverso le fasi del metodo scientifico, dovrà: 

1. Osservare un fenomeno e porsi delle domande.
2. Formulare un'ipotesi, cioè una possibile spiegazione del fenomeno.
3. Fare un esperimento per verificare se l'ipotesi è corretta.
4. Analizzare i risultati.
5. Ripetere l'esperimento anche in modi diversi.
6. Giungere ad una conclusione e formulare una regola.

### Modalità di valutazione

- valutazione del lavoro di gruppo (da parte dell’insegnante attraverso check list)
- valutazione della scheda di progetto consegnata
- autovalutazione del lavoro di gruppo
- valutazione delle competenze acquisite attraverso compiti di realtà

### Positività

- lo studente è messo in situazione
- alta motivazione
- possibilità di decontestualizzare la didattica in altri spazi al di fuori della scuola
- superamento dei preconcetti di fronte alle materie STEM

### Criticità

- mancanza di strutture adeguate
- strumentazione non sempre accessibile e, se esistente, obsoleta
- problemi di sicurezza

## DIDATTICA LABORATORIALE

Nei laboratori l’alunno/a sperimenta se stesso su vari fronti: cognitivo, organizzativo, relazionale, operativo, linguistico, manipolativo, utilizzando l’apporto di diverse discipline, mettendo a fuoco la premessa operativa che permetterà di realizzare la sintesi tra il sapere e il saper fare.
In questa esperienza l’alunno é guidato ad accostare la realtà dai punti di vista offerti dalle discipline, con i loro linguaggi specifici, ad operare connessioni e costruire unità tra i saperi.

### Ruolo del docente

- Indica i riferimenti dei contenuti disciplinari 
- promuove il il "debate"
- facilita, guida, orienta, coordina, affianca
- organizza visite virtuali e reali nel tempo e nello spazio
- spiega l’uso di nuove tecnologie, di software o di metodologie unplugged
- valorizza le competenze digitali e analogiche degli studenti proponendo applicazioni per la propria disciplina

### Ruolo dello studente

- ascoltano, prendono appunti, esprimono dubbi, chiedono spiegazioni, commentano
- confrontano il proprio vissuto esperienziale con quanto proposto
- esplorano il mondo reale e virtuale nel tempo e nello spazio
- si interrogano e formulano ipotesi
- verificano la necessità di nuove informazioni/ricercano informazioni
- verificano ipotesi, connettono informazioni, elaborano progetti
- sperimentano nuove tecnologie, condividono risultati ed esperienze

### Positività

- superare la distanza temporale, spaziale, culturale, etnica, religiosa 
- gestire la propria stessa formazione,
- "apprendere ad apprendere“,
- valorizzare la differente capacità di ciascuno nel rielaborare conoscenze ed elementi di riflessione
- sapersi adattare alla diversità degli altri
- riconoscere l’importanza dei centri di interesse, della loro forma mentis di ciascuno
- prendere consapevolezza dei propri rapporti con la scuola e con la cultura

### Criticità

- Scarsa propensione al cambiamento di metodologie consolidate in anni di insegnamento frontale tradizionale da parte del personale docente
- Scarsa informazione delle famiglie sull’utilizzo delle nuove tecnologie: apocalittici versus integrati.

## Lezione frontale

Si privilegia quando la finalità del momento formativo è la trasmissione dei concetti
Le lezioni frontali possono essere impiegate per l'acquisizione delle conoscenze teoriche mediante uno stile di apprendimento basato sui modelli
Utile quando i partecipanti all'attività formativa siano sprovvisti di elementi conoscitivi rispetto al contenuto trattato

## Lezione dialogata

Nella lezione dialogata l’insegnante attiva il gruppo classe in una azione partecipativa in cui ogni alunno/a contribuisce con specifici compiti a costruire nuovi apprendimenti.
In questo modo si determina una relazione circolare nella quale gli alunni interagiscono non solo con l’insegnante, ma anche tra loro scambiandosi conoscenze, opinioni, ipotesi.

## Brainstorming

Si fornisce un problema
Ogni componente del gruppo classe è libero di proporre la propria soluzione **senza che l'insegnante esprima giudizi**
Tutte le idee sono raccolte ed analizzate fino ad arrivare ad una soluzione

## Cooperative learning

Ogni componente del gruppo classe mette a disposizione il proprio sapere:
- Efficace sul piano cognitivo
- Mette in moto i processi socio-relazionali positivi

Si tratta più che di un metodo didattico di una filosofia, che fa riferimento a pedagogia attiva, costruttivismo, psicologia umanistica e psicologia sociale.
La condizione di interdipendenza positiva determina in ciascuno la constatazione di essere indispensabile per il gruppo, con ricadute positive, non solo sulla motivazione e sull'impegno, ma anche sulla qualità delle relazioni interpersonali.

L'interdipendenza è una necessità per usare con successo l'apprendimento cooperativo. Ci sono diversi modi per strutturare l'interdipendenza nei gruppi, in modo che gli alunni si sentano uniti e impegnati a lavorare insieme.

Elementi di base:

a. Interdipendenza positiva (cooperazione strutturata)
b. Competenze sociali (insegnante)
c. Responsabilità individuale (dei componenti)
d. Monitoraggio (ravvicinato)
e. Valutazione individuale

Si tratta di un'alternativa netta alla didattica frontale. Le lezioni devono essere opportunamente progettate.

## Learning by doing

L'obiettivo diventa **saper fare qualcosa** piuttosto che **conoscere qualcosa**
Si cerca di affinare la strategia per **comprendere** invece di **memorizzare**

## Lavoro di gruppo

Metodologia organizzativa che mira a sviluppare la crescita umana e la socializzazione oltre che le conoscenze e le competenze.
I gruppi possono essere di livello, di compito, elettivi o misti. Possono anche essere scelti in autonomia dal gruppo classe.

## Problem solving

Tecnica didattica che intende l'apprendimento come il risultato di un'attività di scoperta e per soluzione di problemi. Le fasi sono:

- Problem posing
- Raccolta delle informazioni
- Identificazioni delle cause più probabili
- Formulazione di cause possibili
- Sviluppo operativo dell'analisi
- Controllo dei risultati

## Problem posing

Una volta spiegate le caratteristiche e le proprietà di un oggetto, un problema od una situazione, si procede a negarle una alla volta
Attraverso la negazione di un dato certo si instaura un processo di rielaborazione dei problemi

## Problem setting / Problem finding

Si tratta di ragionare sulla priorità in cui vanno inseriti i dati del problema e di saper scegliere i dati da utilizzare e quelli da scartare
Le fasi sono:

- Identificazione di tutti i problemi
- Raccolta di informazioni sui problemi
- Scelta del problema


