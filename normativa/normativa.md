# NORME GENERALI

- Costituzione
- Raccomandazioni UE 18/12/2006 sostituite 2018 (competenze chiave per l'apprendimento permanente)
- Raccomandazioni UE 22/05/2018

## Art. 33 della Costituzione

Sancisce la libertà di insegnamento e pone il vincolo superiore delle norme generali dettate dalla Repubblica.

## Art. 34 della Costituzione

L'accesso all'istruzione scolastica è libero senza alcuna discriminazione di genere ed età, la scuola dell'obbligo, oggi portata fino a 16 anni, è gratuita. La scuola della Repubblica garantisce anche il sostegno e la parità sostanziale agli studenti privi di mezzi, purché i capaci e i meritevoli, mediante borse di studio, assegni ed altre provvidenze da attribuirsi per concorso.

# AUTONOMIA SCOLASTICA

- DPR 275/99: Norme in materia di autonomia delle istituzioni scolastiche
- L. 107/2015: La buona scuola  

## D.P.R. 275/99

- art. 4: autonomia didattica
- art. 5: autonomia organizzativa
- art. 6: autonomia di ricerca
- art. 14: funzionale amministrative di gestione

Ogni istituzione scolastica predispone il piano dell'offerta formativa (POF prima, PTOF dopo 107/2015) che è il **documento costitutivo dell'identità culturale e progettuale ed esplicita la progettazione curricolare, extracurricolare, educativa ed organizzativa di ogni scuola**. Questo deve essere coerente con gli obiettivi generali ed educativi, elaborato dal collegio docenti tenuto conto delle proposte e dei pareri formulati dagli organismi e dalle associazioni anche di fatto dei genitori e, per le scuole secondarie superiori, degli studenti. È pubblico e consegnato agli alunni e alle famiglie all'atto dell'iscrizione.

## L. 107/2015



# NORMATIVA INCLUSIONE

## [Costituzione della Repubblica Italiana](./risorseNormative/Costituzione-1.pdf) (1 Gennaio 1948)

Sancisce i principi alla base di una società inclusiva e di una scuola inclusiva:

- art. 3: uguaglianza formale e sostanziale dei cittadini
- art. 34: la scuola è aperta a tutti
- art. 38: gli inabili ed i minorati hanno diritto all'educazione e all'avviamento professionale

## Relazione Falcucci (1975)

Relazione conclusiva della commissione Falcucci concernente i problemi scolastici degli alunni handicappati

- Magna charta dell’integrazione degli alunni portatori di handicap: vi sono contenuti i principi ispiratori della Legge 517/1977 e della Legge 104/1992.

## Legge 517 - 4 Agosto 1977

Norme sulla valutazione degli alunni e sull’abolizione degli esami di riparazione nonché altre norme di modifica dell’ordinamento scolastico

- Integrazione degli alunni disabili nella scuola pubblica italiana.
- Introduzione di strumenti e risorse per l’effettiva integrazione sostanziale, non solo formale.
- Istituzione dell’insegnante di sostegno specializzato.

## Legge 104 - 5 Febbraio 1992

Legge quadro per l’assistenza, l’integrazione sociale e i diritti delle persone handicappate

- Art. 1: finalità.
- Art. 3: soggetti aventi diritto.
  + c.1: definizione persona handicappata.
  + c.3: situazione con connotazione di gravità.
- Art. 4: accertamento handicap (commissioni mediche).
- Art. 5: diritti generali persona handicappata.
- Art. 10: interventi per persone con handicap in situazione di gravità.
  + c.1: comuni assicurano diritto integrazione sociale e scolastica.
- Art. 12: diritto all’educazione e all’istruzione.
  + c.1: asilo nido.
  + c.2: scuola materna, istituzioni scolastiche di ogni ordine e grado, istituzioni universitarie.
  + c.3: obiettivo integrazione scolastica.
  + c.4: esercizio diritto educazione e istruzione non impedito.
  + c.5: DF *Diagnosi funzionale* – PDF *Profilo dinamico funzionale* – PEI.
  + c.6: verifiche PDF **SOPPRESSO DAL D.LGS. 2017/66**
  + c.8: aggiornamento PDF (conclusione scuola materna, scuola elementare, scuola media, durante istruzione secondaria superiore). **SOPPRESSO DAL D.LGS. 2017/66**
  + c.9: minori ricoverati in situazione di handicap o meno.
- Art. 13: integrazione scolastica.
  + c.1: l’integrazione scolastica della persona handicappata nelle sezioni e classi comuni di ogni ordine e grado e nelle università si realizza mediante:
    - a. programmazione coordinata servizi scolastici, sanitari, socio-assistenziali, culturali, ricreativi, sportivi. Accordi di programma: progetto educativo (scuola) + riabilitativo (usl) + di socializzazione (enti locali).
    - b. dotazione attrezzature tecniche e sussidi didattici.
  + c.3: insegnante specializzato.
  + c.6: contitolarità sezioni e classi.
- Art. 14: modalità di attuazione dell’integrazione.
- Art. 15: gruppi per l’inclusione scolastica. (Gruppo di lavoro interstituzionale regionale, Gruppi per l'inclusione territoriale)
- Art. 16: valutazione del rendimento e prove d’esame.

Diagnosi funzionale
> descrizione analitica della compromissione funzionale dello stato psicofisico dell'alunno in situazione di handicap, al momento in cui accede alla struttura sanitaria per conseguire gli interventi previsti dagli articoli 12 e 13 della legge n. 104 del 1992.

Profilo dinamico funzionale
> il profilo dinamico funzionale è atto successivo alla diagnosi funzionale e indica in via prioritaria, dopo un primo periodo di inserimento scolastico, il prevedibile livello di sviluppo che l'alunno in situazione di handicap dimostra di possedere nei tempi brevi (sei mesi) e nei tempi medi (due anni).

Piano educativo individualizzato
> documento nel quale vengono descritti gli interventi integrati ed equilibrati tra di loro, predisposti per l'alunno in situazione di handicap, in un determinato periodo di tempo, ai fini della realizzazione del diritto all'educazione e all'istruzione, di cui ai primi quattro commi dell'art. 12 della legge n. 104 del 1992. 

## ICF - 2000

Classificazione Internazionale del Funzionamento, della Disabilità e della Salute

## Legge 170 - 2010

Nuove norme in materia di disturbi specifici di apprendimento in ambito scolastico
Definizione dislessia, disortografia, disgrafia, discalculia.

Dislessia
> difficoltà  nell'imparare  a  leggere,  in  particolare  nella  decifrazione  dei  segni  linguistici,  ovvero  nella correttezza e nella rapidità della lettura

Disgrafia
> difficoltà nella realizzazione grafica

Disortografia
> difficoltà nei processi linguistici di transcodifica 

Discalculia
> difficoltà negli automatismi del calcolo e dell'elaborazione dei numeri

- Art. 5: Misure educative e didattiche di supporto

## Direttiva MIUR 27 Dicembre 2012 STRUMENTI INTERVENTO BES

Strumenti d'intervento per alunni con bisogni educativi speciali e organizzazione territoriale per l'inclusione scolastica

## Circolare MIUR 8 - 6 Marzo 2013 INDICAZIONI OPERATIVE BES

La direttiva estende a tutti gli studenti in difficoltà il dirittoalla personalizzazione dell'apprendimento, richiamando la L. 53/2003.  Indicazioni operative Direttiva Ministeriale 27 Dicembre 2012

## D.Lgs. 66 - 13 Aprile 2017

Norme per la promozione dell'inclusione scolastica degli studenti con disabilità, a norma dell'articolo 1, commi 180 e 181, lettera c) della legge 13 Luglio 2015, n. 107
